﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GameServerInterfaces;

namespace GameServer
{
    class GameManager : MarshalByRefObject, GameServerInterfaces.IGameManager
    {
        public GameManager()
        {
            
        }

        public LevelSetAbstract CreateLevelSet()
        {
            return new LevelSet();
        }
    }
}
