﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer
{
    public class GameManagerFactory : MarshalByRefObject, GameServerInterfaces.IGameManagerFactory
    {
        public GameManagerFactory()
        {

        }

        public GameServerInterfaces.IGameManager CreateManager()
        {
            return new GameManager();
        }

        public override object InitializeLifetimeService()
        {
            // truco para que el objeto viva para siempre
            return null;    // que no me vigile nadie para matarme
        }
    }
}
