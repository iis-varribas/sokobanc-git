﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

namespace GameServer
{
    class GameServerMain
    {
        public static void Main(string[] args)
        {
            // configure a channel
            HttpChannel channel = new HttpChannel(2000);
            ChannelServices.RegisterChannel(channel, false);

            // publish the factory
            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(GameManagerFactory),
                "GameManagerFactory",
                WellKnownObjectMode.Singleton);

            Console.WriteLine("Server waiting for requests...");
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
