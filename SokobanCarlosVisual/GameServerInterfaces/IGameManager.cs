﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServerInterfaces
{
    public interface IGameManager
    {
        LevelSetAbstract CreateLevelSet();
    }
}
