﻿using System;
using System.Drawing;

namespace GameServerInterfaces
{
    public abstract class LevelAbstract : MarshalByRefObject
    {
        protected string name = string.Empty; // Name of the level
        protected ItemType[,] levelMap;       // Level layout of items
        protected int nrOfGoals = 0;          // A box must be placed on a goal
        protected int levelNr = 0;
        protected int width = 0;              // Level width in items
        protected int height = 0;             // Level height in items

        // The name of the level set that the level belongs to
        protected string levelSetName = string.Empty;

        protected int moves = 0;              // Sokoban number of moves
        protected int pushes = 0;             // Pushes (when a box is moved)

        protected int sokoPosX;               // X position of Sokoban
        protected int sokoPosY;               // Y position of Sokoban

        protected bool isUndoable = false;    // Indicates if we can do an undo

        // Default direction Sokoban is facing when starting a level.
        protected MoveDirection sokoDirection = MoveDirection.Right;

        // changedItems is updated every time Sokoban moves or pushes a box.
        // Max. 3 items can be changed each push, 2 for each move. We keep
        // track of these change so we don't have to redraw the whole level
        // after each move/push.
        protected Item item1, item2, item3;

        // ITEM_SIZE is the size of an item in the level
        // TO DO: Let user change it, and save the size in the savegame.xml ???
        public const int ITEM_SIZE = 30;

        // Here are 3 items to keep the undo information
        protected Item item1U, item2U, item3U;
        protected int movesBeforeUndo = 0;    // Number of moves before an undo
        protected int pushesBeforeUndo = 0;   // Number of pushes before an undo

        // For drawing the level on screen
        protected Bitmap img;
        protected Graphics g;

        #region Properties

        public string Name
        {
            get { return name; }
        }

        public int LevelNr
        {
            get { return levelNr; }
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        public string LevelSetName
        {
            get { return levelSetName; }
        }

        public int Moves
        {
            get { return moves; }
        }

        public int Pushes
        {
            get { return pushes; }
        }

        public bool IsUndoable
        {
            get { return isUndoable; }
        }

        #endregion

        public abstract Image DrawLevel();
        public abstract Image DrawChanges();
        public abstract bool IsFinished();
        public abstract Image Undo();
        public abstract void MoveSokoban(MoveDirection direction);
    }
}
