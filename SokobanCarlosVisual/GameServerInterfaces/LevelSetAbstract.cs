﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServerInterfaces
{
    public abstract class LevelSetAbstract : MarshalByRefObject
    {
        // Collection of the levels in a XML level set
        protected ArrayList levels = new ArrayList();

        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string email = string.Empty;
        protected string url = string.Empty;
        protected string author = string.Empty;
        protected string filename = string.Empty;

        protected int currentLevel = 0;
        protected int nrOfLevelsInSet = 0;
        protected int lastFinishedLevel = 0;

        #region Properties

        public string Title
        {
            get { return title; }
        }

        public string Description
        {
            get { return description; }
        }

        public string Email
        {
            get { return email; }
        }

        public string Url
        {
            get { return url; }
        }

        public string Author
        {
            get { return author; }
        }

        public string Filename
        {
            get { return filename; }
        }

        public int NrOfLevelsInSet
        {
            get { return nrOfLevelsInSet; }
        }

        public int CurrentLevel
        {
            get { return currentLevel; }
            set { currentLevel = value; }
        }

        public int LastFinishedLevel
        {
            set { lastFinishedLevel = value; }
        }

        #endregion

        public LevelAbstract this[int index]
        {
            get { return (LevelAbstract) levels[index]; }
        }
        public abstract void SetLevelSet(string name);
        public abstract void SetLevelsInLevelSet(string name);
    }
}
