﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServerInterfaces
{
    /// <summary>
    /// Enum for the possible directions of Sokoban
    /// </summary>
    public enum MoveDirection
    {
        Right,
        Up,
        Down,
        Left
    }
}
