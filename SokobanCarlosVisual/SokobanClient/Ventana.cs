﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

using GameServerInterfaces;

namespace GameServerInterfaces
{
    /// <summary>
    /// This class draws everything on screen. It also handles user input.
    /// </summary>
    public class Board : System.Windows.Forms.Form
    {
        private System.ComponentModel.Container components = null;

        // Initialize the required objects
        private LevelSetAbstract levelSet;
        private LevelAbstract level;

        // Objects for drawing graphics on screen
        private PictureBox screen;
        private Image img;


        /// <summary>
        /// Constructor
        /// </summary>
        public Board()
        {
            InitializeComponent();
            screen = new PictureBox();
            Controls.AddRange(new Control[] { screen });

            InitializeGame();
        }


        /// <summary>
        /// Sets the data for PlayerData, LevelSet, etc..
        /// </summary>
        private void InitializeGame()
        {
            // en este punto comunicamos con el servidor
            HttpChannel channel = new HttpChannel();
            ChannelServices.RegisterChannel(channel, false);

            IGameManagerFactory factory = (IGameManagerFactory) Activator.GetObject(
                typeof(IGameManagerFactory),
                "http://localhost:2000/GameManagerFactory");
            IGameManager manager = factory.CreateManager();
            levelSet = manager.CreateLevelSet();

            levelSet.CurrentLevel = 1;

            // Load the levels in the LevelSet object and set the current level
            levelSet.SetLevelsInLevelSet("boxworld.xml");
            level = (LevelAbstract) levelSet[levelSet.CurrentLevel - 1];

            // Draw the level on the screen
            DrawLevel();
            /////////////// OLD CODE
            /*levelSet = new LevelSet();
            levelSet.CurrentLevel = 1;

            // Load the levels in the LevelSet object and set the current level
            levelSet.SetLevelsInLevelSet("boxworld.xml");
            level = (LevelAbstract) levelSet[levelSet.CurrentLevel - 1];

            // Draw the level on the screen
            DrawLevel();*/
        }


        /// <summary>
        /// This method sets the width and the height of the screen,
        /// according to the level width and height. It then lets the level
        /// itself. Lastly, we set the labels to display the player/level info.
        /// </summary>
        private void DrawLevel()
        {
            int levelWidth = (level.Width) * LevelAbstract.ITEM_SIZE;
            int levelHeight = (level.Height) * LevelAbstract.ITEM_SIZE;

            this.ClientSize = new Size(levelWidth, levelHeight);
            screen.Size = new System.Drawing.Size(levelWidth, levelHeight);

            img = level.DrawLevel();
            screen.Image = img;
        }


        /// <summary>
        /// After moving Sokoban we only draw the changes on the level, and not
        /// redraw the whole level
        /// </summary>
        private void DrawChanges()
        {
            img = level.DrawChanges();
            screen.Image = img;
        }

        /// <summary>
        /// Reads input from the keyboard and does something depending on what
        /// key is pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AKeyDown(object sender, KeyEventArgs e)
        {
            string result = e.KeyData.ToString();

            switch (result)
            {
                case "Up":
                    level.MoveSokoban(MoveDirection.Up);
                    break;
                case "Down":
                    level.MoveSokoban(MoveDirection.Down);
                    break;
                case "Right":
                    level.MoveSokoban(MoveDirection.Right);
                    break;
                case "Left":
                    level.MoveSokoban(MoveDirection.Left);
                    break;
                default:
                    return;
            }
            // Draw the changes of the level
            DrawChanges();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new Board());
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // Board
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(84)), ((System.Byte)(48)), ((System.Byte)(12)));
            this.ClientSize = new System.Drawing.Size(446, 200);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Práctica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Esquema para la práctica de URJC";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AKeyDown);
            this.ResumeLayout(false);

        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #endregion
    }
}
