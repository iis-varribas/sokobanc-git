using System;
using System.Net;



// IMPORTANT: need System.Core assembly in this proyect and whose wats to use it.

namespace UserDefined.Extensions.Http
{
	
	public static partial class Extension
	{
		/// <summary>
		/// Http POST method.
		/// Throws a WebException on fail.
		/// </summary>
		/// <param name="URI">
		/// A <see cref="System.String"/>
		/// </param>
		/// <param name="ContentType">
		/// A <see cref="System.String"/>
		/// </param>
		/// <param name="Parameters">
		/// A <see cref="System.String"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.String"/>
		/// </returns>
		public static string POST(this string URI, string ContentType, string Parameters)
		{
			using(WebClient wc = new WebClient())
			{
				wc.Headers[HttpRequestHeader.ContentType] = ContentType;
				string HtmlResult = wc.UploadString(URI, "POST", Parameters);
				return HtmlResult.Trim();
			}
		}
		
		/// <summary>
		/// Http GET method.
		/// Throws a WebException on fail.
		/// </summary>
		/// <param name="URI">
		/// A <see cref="System.String"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.String"/>
		/// </returns>
		public static string GET(this string URI)
		{
			using(WebClient wc = new WebClient())
			{
				string HtmlResult = wc.DownloadString(URI);
				return HtmlResult.Trim();
			}
		}
		
		/// <summary>
		/// Http DELETE method.
		/// Return true if successfully deleted.
		/// </summary>
		/// <param name="URI">
		/// A <see cref="System.String"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.Boolean"/>
		/// </returns>
		public static bool DELETE(this string URI)
		{
			HttpWebRequest request = (HttpWebRequest) HttpWebRequest.Create(URI);
			request.Method = "DELETE";
			HttpWebResponse  response = (HttpWebResponse) request.GetResponse();
			HttpStatusCode status = response.StatusCode;
			return status.Equals(HttpStatusCode.OK) || status.Equals(HttpStatusCode.Accepted) || status.Equals(HttpStatusCode.NoContent);
		}
	}
	
	public static partial class Extension
	{
		public static string POST(this Uri URI, string ContentType, string Parameters)
		{
			using(WebClient wc = new WebClient())
			{
				wc.Headers[HttpRequestHeader.ContentType] = ContentType;
				string HtmlResult = wc.UploadString(URI, "POST", Parameters);
				return HtmlResult.Trim();
			}
		}
		
		
		public static string GET(this Uri URI)
		{
			using(WebClient wc = new WebClient())
			{
				string HtmlResult = wc.DownloadString(URI);
				return HtmlResult.Trim();
			}
		}
		
		
		public static bool DELETE(this Uri URI)
		{
			HttpWebRequest request = (HttpWebRequest) HttpWebRequest.Create(URI);
			request.Method = "DELETE";
			HttpWebResponse  response = (HttpWebResponse) request.GetResponse();
			HttpStatusCode status = response.StatusCode;
			return status.Equals(HttpStatusCode.OK) || status.Equals(HttpStatusCode.Accepted) || status.Equals(HttpStatusCode.NoContent);
		}
		
	}
	
}

















