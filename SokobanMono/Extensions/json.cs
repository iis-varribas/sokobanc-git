using System;
using System.Web.Script.Serialization;

// IMPORTANT: need System.Core assembly in this proyect and whose wats to use it.



namespace UserDefined.Extensions.JSON
{

	public static partial class Extension
	{
		public static string ToJSON(this object o)
		{
			JavaScriptSerializer jss = new JavaScriptSerializer();
			return jss.Serialize(o);
		}
		
		public static object ParseJSON(this string sJOSN)
		{
			JavaScriptSerializer jss = new JavaScriptSerializer();
			return jss.DeserializeObject(sJOSN);
		}
		
		
	}
}

