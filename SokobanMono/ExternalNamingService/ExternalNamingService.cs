using System;
using System.Configuration;
using UserDefined.Extensions.Http;
using UserDefined.Extensions.JSON;

namespace UserDefined.Naming
{
	public static class ExternalNamingService
	{
		public static string URL = "http://host:port/ServicioNombres/webresources/entities.servicio";
		
		
		static ExternalNamingService()
		{
			LoadFromConfig();	
		}
		
		
		public static string ConvertToExternal(string localhost_uri)
		{
			string public_ip = "http://automation.whatismyip.com/n09230945.asp".GET();
			return localhost_uri.Replace("localhost", public_ip);
		}
		
		public static string LookUp(string id)
		{
			Console.WriteLine("[ExternalNamingService] LookUp()");
			Console.WriteLine("  "+URL+"/"+id);
			string reply = (URL+"/"+id).GET();
			object obj = reply.ParseJSON();
			
			System.Collections.IDictionary mapping = (System.Collections.IDictionary) obj;
			string url = (string)mapping["url"];
			Console.WriteLine("  "+url);
			return url;
		}
		
		public static bool Register(string id, string uri)
		{
			Console.Write("[ExternalNamingService] Register({0}, {1})... ", id, uri);
			object register_obj = new { name = id, url = uri};
			try{
				URL.POST("application/json", register_obj.ToJSON());
				Console.WriteLine("Ok");
				return true;
			}catch(Exception){
				Console.WriteLine("Failed");
				return false;
			}
		}
		
		public static bool Deregister(string id)
		{
			Console.Write("[ExternalNamingService] Deegister({0})... ", id);
			try{
				bool r = (URL+"/"+id).DELETE();
				Console.WriteLine("Ok");
				return r;
			}catch(Exception){
				Console.WriteLine("Failed");
				return false;
			}
		}
		
		
		private static void LoadFromConfig()
		{
			Console.WriteLine("Setup 'ExternalNamingService'");
			
//			var configfile = new ExeConfigurationFileMap{ ExeConfigFilename = @"naming.2.config"};
//			Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configfile, ConfigurationUserLevel.None);
			
			// Load the default App config file (named arg[0].config, ej: myprogram.exe.config)
			Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//			config.SaveAs("naming.2.config", ConfigurationSaveMode.Full);
			
			Console.WriteLine("  File: "+config.FilePath);
			
			if (config.AppSettings.Settings["ExternalNamingService"] == null)
			{
				config.AppSettings.Settings.Add("ExternalNamingService", URL);
				config.Save(ConfigurationSaveMode.Modified);
				ConfigurationManager.RefreshSection("appSettings");
			}else{
				URL = config.AppSettings.Settings["ExternalNamingService"].Value;
			}
			Console.WriteLine("  Using '{0}'", URL);
			
//            foreach (string key in ConfigurationManager.AppSettings)
//            {
//                string value = ConfigurationManager.AppSettings[key];
//                Console.WriteLine("Key: {0}, Value: {1}", key, value);
//            }

		}

	}
	
}

