﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Remoting.Lifetime;

using RemotingLifeTimeTools;
using Sokoban.GameServer.Interfaces;
using Sokoban.Library;
using UserDefined.Extensions.Http;
using UserDefined.Naming;

namespace Sokoban.GameServer
{
    class GameManager : MarshalByRefObjectWithLease, IGameManager
    {
		public static readonly string STORE_PATH = "levelsets";
		private string levelSetDonwloadWebService;
		
        public GameManager()
        {
            Console.WriteLine("GameManager.__NEW__()");
			if (!System.IO.Directory.Exists(STORE_PATH))
			{
				System.IO.Directory.CreateDirectory(STORE_PATH);
			}
			
			ConnectToLevelSetProvider();
        }
		
        ~GameManager()
        {
            Console.WriteLine("GameManager.__DESTROY__()");
        }
		
		
		public LevelSetAbstract GetLevelSet(string name)
        {
			string file = System.IO.Path.Combine(STORE_PATH, name+".xml");
			LevelSet levelSet = null; 
			if (!System.IO.File.Exists(file)) {
				if (levelSetDonwloadWebService == null) {
					Console.WriteLine(" LevelSet '{0}' is not in cache", name);
					return null;
				}
				Console.WriteLine("Request for LevelSet {0}", name);
				string uri = levelSetDonwloadWebService+"/"+name;
				try{
					string levelSet_xml = uri.GET();
					Console.WriteLine("Level got");
					System.IO.File.WriteAllText(file, levelSet_xml);
					Console.WriteLine("Level written");
				}catch(Exception){
					Console.WriteLine("Level not recovered");
					return null;
				}
			}
			levelSet = new LevelSet();
			levelSet.SetLevelSet(file);
			Console.WriteLine("LevelSet {0} dispatched", name);

            return levelSet;
        }
		
		
        public override object InitializeLifetimeService()
        {
            // truco para que el objeto viva para siempre
			// que no me vigile nadie para matarme
			// Muy malo porque el objeto nunca será destruido
			// hasta que se reinicie el ordenador
            // return null;    
			
			ILease lease = (ILease)base.InitializeLifetimeService();
    		if (lease.CurrentState == LeaseState.Initial)
			{
				lease.InitialLeaseTime = TimeSpan.FromMinutes(5);
				lease.SponsorshipTimeout = TimeSpan.FromSeconds(10);
				lease.RenewOnCallTime = TimeSpan.FromSeconds(10);
			}
			Console.WriteLine("Lease: State [{0}] Time [{1}]", lease.CurrentState, lease.CurrentLeaseTime);
			
    		return lease;
        }
		

		
		private void ConnectToLevelSetProvider()
		{
			Console.WriteLine("ConnectToLevelSetProvider()");
			
			try{
			levelSetDonwloadWebService = ExternalNamingService.LookUp("ServidorSeries1.web");
			}catch (WebException){
				Console.WriteLine("[ERROR] Failed to connect to LevelSetProvider. Using cached levels");
				levelSetDonwloadWebService = null;
			}
			//TODO System.Net.WebException
		}
    }
}
