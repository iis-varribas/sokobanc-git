﻿using System;
using System.Runtime.Remoting.Lifetime;

using Sokoban.GameServer.Interfaces;

namespace Sokoban.GameServer
{
    public class GameManagerFactory : MarshalByRefObject, IGameManagerFactory
    {
        public GameManagerFactory()
        {
			Console.WriteLine("GameManagerFactory.__NEW__()");
        }
		
        ~GameManagerFactory()
        {
            Console.WriteLine("GameManagerFactory.__DESTROY__()");
        }
		

        public IGameManager CreateManager()
        {
			Console.WriteLine("New GameManager dispatched");
			GameManager manager = new GameManager();
            return manager;
        }

    }
}
