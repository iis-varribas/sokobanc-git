﻿using System;

using Sokoban.Library;


namespace Sokoban.GameServer
{
    /// <summary>
    /// Level class. Keeps the level information and draws a level on screen.
    /// An 'item' in a level can be a wall, a floor, a box or sokoban. The
    /// width and height of a level are measured in items, not in pixels. The
    /// width and height of a level when drawed on the screen can be calculated
    /// by multiplying the width and height by the size of the item.
    /// </summary>
    public class Level : LevelAbstract
	{
    
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="aName">Level name</param>
        /// <param name="aLevelMap">Level map</param>
        /// <param name="aWidth">Level width</param>
        /// <param name="aHeight">Level height</param>
        /// <param name="aNrOfGoals">Number of goals</param>
        /// <param name="aLevelNr">Level number</param>
        /// <param name="aLevelSetName">Set name that level belongs to</param>
        public Level(string aName, 
		             ItemType[,] aLevelMap, 
		             int aWidth, int aHeight, 
		             int sPosX, int sPosY, 
		             int aNrOfGoals, int aLevelNr, 
		             string aLevelSetName
		) : base(aName, aLevelMap, aWidth, aHeight, sPosX, sPosY, aNrOfGoals, aLevelNr, aLevelSetName)
		{
		}

		public override object Clone()
		{
			return new Level(Name, _levelMap, Width, Height, _sokoPosX, _sokoPosY, NrOfGoals, LevelNr, LevelSetName	);
		}


        /// <summary>
        /// Checks if a level is finished/solved. A level is solved when all
        /// boxes has been places on all goals. So all we have to do is count
        /// the number of boxes placed on a goal and compare this with the
        /// total number of goals defined for a level.
        /// </summary>
        /// <returns>True if level is solved, otherwise false</returns>
        public override bool IsFinished()
        {
            int nrOfPackagesOnGoal = 0;

            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    if (levelMap[i, j] == ItemType.PackageOnGoal)
                        nrOfPackagesOnGoal++;

            return nrOfPackagesOnGoal == nrOfGoals; 
        }


        

        #region Moving Sokoban

        /// <summary>
        /// Check in what direction we want to move and call the corresponding
        /// method.
        /// </summary>
        /// <param name="direction">Direction to move in</param>
        public override bool MoveSokoban(MoveDirection direction)
        {
            sokoDirection = direction;

            switch (direction)
            {
                case MoveDirection.Up:
                    return MoveUp();
                case MoveDirection.Down:
                    return MoveDown();
                case MoveDirection.Right:
                    return MoveRight();
                case MoveDirection.Left:
                    return MoveLeft();
				default:
					return false;
            }
        }
		
		
		public override void Undo(){}
		public override void Redo(){}


        // Here's what happens in the 4 move methods:
        // If the item in front of Sokoban is a box, we first have to check if
        // the item next to the box is a free space (floor or goal). If so, we
        // can move the box. Else, nothing will happen. Then we update the
        // levelmap. Note that the box as well as Sokoban can be standing on a
        // free floor or on a goal. Before moving Sokoban we also set the
        // current position of Sokoban, the box, and the free space thereafter
        // so we're able to undo the move.
        // If there's a free space in front of Sokoban, we can simply move him
        // one step. If Sokoban can't move in the desired direction (there's a
        // wall or no free space behind a box), nothing happens here.
        // At most, we have 3 items in the level that have changed. We put them
        // in 3 Item objects and redraw these after moving Sokoban. This way,
        // we don't have to redraw the whole level after each move, which
        // results in a huge performance improvement.
        // Lastly, we update the number of moves and pushes. Before this, we
        // set the movesBeforeUndo and pushesBeforeUndo to the current number
        // moves and pushes so we can restore these values when we undo a move.

        /// <summary>
        /// Move up
        /// </summary>
        private bool MoveUp()
        {
			bool hasMove = true;
            if ((levelMap[sokoPosX, sokoPosY - 1] == ItemType.Package ||
                levelMap[sokoPosX, sokoPosY - 1] == ItemType.PackageOnGoal) &&
                (levelMap[sokoPosX, sokoPosY - 2] == ItemType.Floor ||
                levelMap[sokoPosX, sokoPosY - 2] == ItemType.Goal))
            {

                if (levelMap[sokoPosX, sokoPosY - 2] == ItemType.Floor)
                {
                    levelMap[sokoPosX, sokoPosY - 2] = ItemType.Package;
                    item3 = new Item(ItemType.Package, sokoPosX, sokoPosY - 2);
                }
                else if (levelMap[sokoPosX, sokoPosY - 2] == ItemType.Goal)
                {
                    levelMap[sokoPosX, sokoPosY - 2] = ItemType.PackageOnGoal;
                    item3 = new Item(ItemType.PackageOnGoal, sokoPosX, sokoPosY - 2);
                }
                if (levelMap[sokoPosX, sokoPosY - 1] == ItemType.Package)
                {
                    levelMap[sokoPosX, sokoPosY - 1] = ItemType.Sokoban;
                    item2 = new Item(ItemType.Sokoban, sokoPosX, sokoPosY - 1);
                }
                else if (levelMap[sokoPosX, sokoPosY - 1] == ItemType.PackageOnGoal)
                {
                    levelMap[sokoPosX, sokoPosY - 1] = ItemType.SokobanOnGoal;
                    item2 = new Item(ItemType.SokobanOnGoal, sokoPosX, sokoPosY - 1);
                }

                UpdateCurrentSokobanPosition();
                moves++;
                pushes++;
                sokoPosY--;
            }
            else if (levelMap[sokoPosX, sokoPosY - 1] == ItemType.Floor ||
                levelMap[sokoPosX, sokoPosY - 1] == ItemType.Goal)
            {
                if (levelMap[sokoPosX, sokoPosY - 1] == ItemType.Floor)
                {
                    levelMap[sokoPosX, sokoPosY - 1] = ItemType.Sokoban;
                    item2 = new Item(ItemType.Sokoban, sokoPosX, sokoPosY - 1);
                }
                else if (levelMap[sokoPosX, sokoPosY - 1] == ItemType.Goal)
                {
                    levelMap[sokoPosX, sokoPosY - 1] = ItemType.SokobanOnGoal;
                    item2 = new Item(ItemType.SokobanOnGoal, sokoPosX, sokoPosY - 1);
                }

                item3 = null;
                UpdateCurrentSokobanPosition();
                moves++;
                sokoPosY--;
            }
			else
			{
				hasMove = false;
			}
			return hasMove;
        }


        /// <summary>
        /// Move down
        /// </summary>
        private bool MoveDown()
        {
			bool hasMove = true;
            if ((levelMap[sokoPosX, sokoPosY + 1] == ItemType.Package ||
                levelMap[sokoPosX, sokoPosY + 1] == ItemType.PackageOnGoal) &&
                (levelMap[sokoPosX, sokoPosY + 2] == ItemType.Floor ||
                levelMap[sokoPosX, sokoPosY + 2] == ItemType.Goal))
            {

                if (levelMap[sokoPosX, sokoPosY + 2] == ItemType.Floor)
                {
                    levelMap[sokoPosX, sokoPosY + 2] = ItemType.Package;
                    item3 = new Item(ItemType.Package, sokoPosX, sokoPosY + 2);
                }
                else if (levelMap[sokoPosX, sokoPosY + 2] == ItemType.Goal)
                {
                    levelMap[sokoPosX, sokoPosY + 2] = ItemType.PackageOnGoal;
                    item3 = new Item(ItemType.PackageOnGoal, sokoPosX, sokoPosY + 2);
                }

                if (levelMap[sokoPosX, sokoPosY + 1] == ItemType.Package)
                {
                    levelMap[sokoPosX, sokoPosY + 1] = ItemType.Sokoban;
                    item2 = new Item(ItemType.Sokoban, sokoPosX, sokoPosY + 1);
                }
                else if (levelMap[sokoPosX, sokoPosY + 1] == ItemType.PackageOnGoal)
                {
                    levelMap[sokoPosX, sokoPosY + 1] = ItemType.SokobanOnGoal;
                    item2 = new Item(ItemType.SokobanOnGoal, sokoPosX, sokoPosY + 1);
                }

                UpdateCurrentSokobanPosition();
                moves++;
                pushes++;
                sokoPosY++;
            }
            else if (levelMap[sokoPosX, sokoPosY + 1] == ItemType.Floor ||
                levelMap[sokoPosX, sokoPosY + 1] == ItemType.Goal)
            {
                if (levelMap[sokoPosX, sokoPosY + 1] == ItemType.Floor)
                {
                    levelMap[sokoPosX, sokoPosY + 1] = ItemType.Sokoban;
                    item2 = new Item(ItemType.Sokoban, sokoPosX, sokoPosY + 1);
                }
                else if (levelMap[sokoPosX, sokoPosY + 1] == ItemType.Goal)
                {
                    levelMap[sokoPosX, sokoPosY + 1] = ItemType.SokobanOnGoal;
                    item2 = new Item(ItemType.SokobanOnGoal, sokoPosX, sokoPosY + 1);
                }

                item3 = null;
                UpdateCurrentSokobanPosition();
                moves++;
                sokoPosY++;
            }
			else
			{
				hasMove = false;
			}
			return hasMove;
        }


        /// <summary>
        /// Move right
        /// </summary>
        private bool MoveRight()
        {
			bool hasMove = true;
            if ((levelMap[sokoPosX + 1, sokoPosY] == ItemType.Package ||
                levelMap[sokoPosX + 1, sokoPosY] == ItemType.PackageOnGoal) &&
                (levelMap[sokoPosX + 2, sokoPosY] == ItemType.Floor ||
                levelMap[sokoPosX + 2, sokoPosY] == ItemType.Goal))
            {

                if (levelMap[sokoPosX + 2, sokoPosY] == ItemType.Floor)
                {
                    levelMap[sokoPosX + 2, sokoPosY] = ItemType.Package;
                    item3 = new Item(ItemType.Package, sokoPosX + 2, sokoPosY);
                }
                else if (levelMap[sokoPosX + 2, sokoPosY] == ItemType.Goal)
                {
                    levelMap[sokoPosX + 2, sokoPosY] = ItemType.PackageOnGoal;
                    item3 = new Item(ItemType.PackageOnGoal, sokoPosX + 2, sokoPosY);
                }
                if (levelMap[sokoPosX + 1, sokoPosY] == ItemType.Package)
                {
                    levelMap[sokoPosX + 1, sokoPosY] = ItemType.Sokoban;
                    item2 = new Item(ItemType.Sokoban, sokoPosX + 1, sokoPosY);
                }
                else if (levelMap[sokoPosX + 1, sokoPosY] == ItemType.PackageOnGoal)
                {
                    levelMap[sokoPosX + 1, sokoPosY] = ItemType.SokobanOnGoal;
                    item2 = new Item(ItemType.SokobanOnGoal, sokoPosX + 1, sokoPosY);
                }

                UpdateCurrentSokobanPosition();
                moves++;
                pushes++;
                sokoPosX++;
            }
            else if (levelMap[sokoPosX + 1, sokoPosY] == ItemType.Floor ||
                levelMap[sokoPosX + 1, sokoPosY] == ItemType.Goal)
            {
                if (levelMap[sokoPosX + 1, sokoPosY] == ItemType.Floor)
                {
                    levelMap[sokoPosX + 1, sokoPosY] = ItemType.Sokoban;
                    item2 = new Item(ItemType.Sokoban, sokoPosX + 1, sokoPosY);
                }
                else if (levelMap[sokoPosX + 1, sokoPosY] == ItemType.Goal)
                {
                    levelMap[sokoPosX + 1, sokoPosY] = ItemType.SokobanOnGoal;
                    item2 = new Item(ItemType.SokobanOnGoal, sokoPosX + 1, sokoPosY);
                }

                item3 = null;
                UpdateCurrentSokobanPosition();
                moves++;
                sokoPosX++;
            }
			else
			{
				hasMove = false;
			}
			return hasMove;
        }


        /// <summary>
        /// Move left
        /// </summary>
        private bool MoveLeft()
        {
			bool hasMove = true;
            if ((levelMap[sokoPosX - 1, sokoPosY] == ItemType.Package ||
                levelMap[sokoPosX - 1, sokoPosY] == ItemType.PackageOnGoal) &&
                (levelMap[sokoPosX - 2, sokoPosY] == ItemType.Floor ||
                levelMap[sokoPosX - 2, sokoPosY] == ItemType.Goal))
            {

                if (levelMap[sokoPosX - 2, sokoPosY] == ItemType.Floor)
                {
                    levelMap[sokoPosX - 2, sokoPosY] = ItemType.Package;
                    item3 = new Item(ItemType.Package, sokoPosX - 2, sokoPosY);
                }
                else if (levelMap[sokoPosX - 2, sokoPosY] == ItemType.Goal)
                {
                    levelMap[sokoPosX - 2, sokoPosY] = ItemType.PackageOnGoal;
                    item3 = new Item(ItemType.PackageOnGoal, sokoPosX - 2, sokoPosY);
                }
                if (levelMap[sokoPosX - 1, sokoPosY] == ItemType.Package)
                {
                    levelMap[sokoPosX - 1, sokoPosY] = ItemType.Sokoban;
                    item2 = new Item(ItemType.Sokoban, sokoPosX - 1, sokoPosY);
                }
                else if (levelMap[sokoPosX - 1, sokoPosY] == ItemType.PackageOnGoal)
                {
                    levelMap[sokoPosX - 1, sokoPosY] = ItemType.SokobanOnGoal;
                    item2 = new Item(ItemType.SokobanOnGoal, sokoPosX - 1, sokoPosY);
                }

                UpdateCurrentSokobanPosition();
                moves++;
                pushes++;
                sokoPosX--;
            }
            else if (levelMap[sokoPosX - 1, sokoPosY] == ItemType.Floor ||
                levelMap[sokoPosX - 1, sokoPosY] == ItemType.Goal)
            {
                if (levelMap[sokoPosX - 1, sokoPosY] == ItemType.Floor)
                {
                    levelMap[sokoPosX - 1, sokoPosY] = ItemType.Sokoban;
                    item2 = new Item(ItemType.Sokoban, sokoPosX - 1, sokoPosY);
                }
                else if (levelMap[sokoPosX - 1, sokoPosY] == ItemType.Goal)
                {
                    levelMap[sokoPosX - 1, sokoPosY] = ItemType.SokobanOnGoal;
                    item2 = new Item(ItemType.SokobanOnGoal, sokoPosX - 1, sokoPosY);
                }

                item3 = null;
                UpdateCurrentSokobanPosition();
                moves++;
                sokoPosX--;
            }
			else
			{
				hasMove = false;
			}
			return hasMove;
        }


        /// <summary>
        /// Updates Sokoban's position. This code is used in all the MoveXX
        /// methods, so I put it in a separate method.
        /// </summary>
        private void UpdateCurrentSokobanPosition()
        {
            if (levelMap[sokoPosX, sokoPosY] == ItemType.Sokoban)
            {
                levelMap[sokoPosX, sokoPosY] = ItemType.Floor;
                item1 = new Item(ItemType.Floor, sokoPosX, sokoPosY);
            }
            else if (levelMap[sokoPosX, sokoPosY] == ItemType.SokobanOnGoal)
            {
                levelMap[sokoPosX, sokoPosY] = ItemType.Goal;
                item1 = new Item(ItemType.Goal, sokoPosX, sokoPosY);
            }

            Console.WriteLine("Updated Sokoban position");
        }

        #endregion


    }
}
