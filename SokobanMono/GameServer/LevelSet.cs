﻿using System;
using System.Collections;
using System.Xml;

using Sokoban.Library;

namespace Sokoban.GameServer
{
    /// <summary>
    /// LevelSet contains information about the level set we can play. This
    /// level set information is stored in an XML file.
    /// </summary>
    public class LevelSet : LevelSetAbstract
    {
		private bool levelsAreLoaded = false;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="aTitle">Level set title</param>
        /// <param name="aDescription">Level set description</param>
        /// <param name="aEmail">Email of the author of the level set</param>
        /// <param name="aUrl">Email of the author's website</param>
        /// <param name="aAuthor">Name of the author</param>
        /// <param name="aNrOfLevels">Number of levels in level set</param>
        /// <param name="aFilename">Path + filename of the level set</param>
        public LevelSet(string aTitle, string aDescription, string aEmail,
            string aUrl, string aAuthor, int aNrOfLevels, string aFilename)
        {
            title = aTitle;
            description = aDescription;
            email = aEmail;
            url = aUrl;
            author = aAuthor;
            nrOfLevelsInSet = aNrOfLevels;
            filename = aFilename;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public LevelSet() { }


        /// <summary>
        /// Indexer for the LevelSet object
        /// </summary>
        public override LevelAbstract this[int index]
        {
            get 
			{ 
				if (!levelsAreLoaded){
					Console.WriteLine("Loading Levels ["+title+"] (level "+index+" requested)");
					this.SetLevelsInLevelSet(filename);
				}
				return base[index]; 
			}
        }


        /// <summary>
        /// Sets the general information of the level set.
        /// </summary>
        /// <param name="setName"></param>
        public void SetLevelSet(string setName)
        {
            // Load XML into memory
            XmlDocument doc = new XmlDocument();
            doc.Load(setName);
			levelsAreLoaded = false;
            filename = setName;
            title =  doc.SelectSingleNode("//Title").InnerText;
			//FIXME
//            description = doc.SelectSingleNode("//Description").InnerText;
//            email = doc.SelectSingleNode("//Email").InnerText;
//            url = doc.SelectSingleNode("//Url").InnerText;

//            XmlNode levelCollection = doc.SelectSingleNode("//LevelCollection");
//            author = levelCollection.Attributes["Copyright"].Value;
            XmlNodeList levels = doc.SelectNodes("//Level");
            nrOfLevelsInSet = levels.Count;
        }


        /// <summary>
        /// Reads all the Level elements from the level set. This method is
        /// called when we have selected a level set that we want to play (or
        /// we've read the level set from the savegame when we're continuing a
        /// previously saved game.
        /// </summary>
        /// <param name="setName"></param>
        private void SetLevelsInLevelSet(string setName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(setName);

            // Get all Level elements from the level set
            XmlNodeList levelInfoList = doc.SelectNodes("//Level");

			
			levels.Clear();
			nrOfLevelsInSet = 0;
			
            int levelNr = nrOfLevelsInSet;
            foreach (XmlNode levelInfo in levelInfoList)
            {
                LoadLevel(levelInfo, levelNr++);
            }
			nrOfLevelsInSet += levelNr;
			levelsAreLoaded = true;
        }


        /// <summary>
        /// Reads the level information from a particular Level node
        /// </summary>
        /// <param name="levelInfo">The level node</param>
        /// <param name="levelNr">Level number</param>
        private void LoadLevel(XmlNode levelInfo, int levelNr)
        {
            // Read the attributes from the level element            
            XmlAttributeCollection xac = levelInfo.Attributes;
            string levelName = xac["Id"].Value;
            int levelWidth = int.Parse(xac["Width"].Value);
            int levelHeight = int.Parse(xac["Height"].Value);
            int nrOfGoals = 0;

            // Read the layout of the level
            XmlNodeList levelLayout = levelInfo.SelectNodes("L");

            // Declare the level map
            ItemType[,] levelMap = new ItemType[levelWidth, levelHeight];
			
			//Detect Soko position
			int sokoPosX = 0;
			int sokoPosY = 0;

            // Read the level line by line
            for (int i = 0; i < levelHeight; i++)
            {
                string line = levelLayout[i].InnerText;
                bool wallEncountered = false;

                // Read the line character by character
                for (int j = 0; j < levelWidth; j++)
                {
                    // If the end of the line is shorter than the width of the
                    // level, then the rest of the line is filled with spaces.
                    if (j >= line.Length)
                        levelMap[j, i] = ItemType.Space;
                    else
                    {
                        switch (line[j].ToString())
                        {
                            case " ":
                                if (wallEncountered)
                                    levelMap[j, i] = ItemType.Floor;
                                else
                                    levelMap[j, i] = ItemType.Space;
                                break;
                            case "#":
                                levelMap[j, i] = ItemType.Wall;
                                wallEncountered = true;
                                break;
                            case "$":
                                levelMap[j, i] = ItemType.Package;
                                break;
                            case ".":
                                levelMap[j, i] = ItemType.Goal;
                                nrOfGoals++;
                                break;
                            case "@":
                                levelMap[j, i] = ItemType.Sokoban;
								sokoPosX = j;
								sokoPosY = i;
                                break;
                            case "*":
                                levelMap[j, i] = ItemType.PackageOnGoal;
                                nrOfGoals++;
                                break;
                            case "+":
                                levelMap[j, i] = ItemType.SokobanOnGoal;
                                nrOfGoals++;
                                break;
                            case "=":
                                levelMap[j, i] = ItemType.Space;
                                break;
                        }
                    }
                }
            }

            // Add a new level to the collection of levels in the level set.
            levels.Add(new Level(levelName, levelMap, 
			                     levelWidth, levelHeight, 
			                     sokoPosX, sokoPosY,
			                     nrOfGoals, 
			                     levelNr, title));
        }

		private string MiniInvoke(EventHandler h)
		{
			try{
				return (string) h.DynamicInvoke(null);
			}catch(Exception){
				return "";
			}
		}
    }
}
