using System;
using UserDefined.Naming;
namespace DatabaseInterfaces
{
	// Statics Objects have a lifetime like Singleton remote objects.
	// The object is unique (singleton), and it will be created only when called.
	public static class Database
	{
		
		// If ExternalNamingService.LookUp() fails (raises an exception) Database constructor fails.
		// Then TypeInitializationException will be throwed.
		// "System.TypeInitializationException was thrown by the type initializer for DatabaseInterfaces.Database"
		private static string remoteObjectURI;
		static Database()
		{
			remoteObjectURI = ExternalNamingService.LookUp("DatabaseInterfaces.rem");
		}
		
		public static IDatabaseConnector GetConnection()
		{
            object obj = Activator.GetObject(
					typeof(IDatabaseConnector),
					remoteObjectURI);
			
			IDatabaseConnector dc = (IDatabaseConnector) obj;
			
//			// For debug purposes			
//			Console.WriteLine("New IDatabaseConnector dispatched");
//			Console.WriteLine(dc.UserCount());
//			Console.WriteLine("DB proved");
			
			return dc;
			
		}
	}
}

