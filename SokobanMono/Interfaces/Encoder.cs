﻿using System;
using System.Text;

using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace DatabaseInterfaces
{
    public static class Encoder
    {
        public static string EncodePassword(string originalPassword)
        {
            //Declarations
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;

            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            encodedBytes = md5.ComputeHash(originalBytes);

            //Convert encoded bytes back to a 'readable' string
            return Regex.Replace(BitConverter.ToString(encodedBytes), "-", "").ToLower();
        }
    }
}
