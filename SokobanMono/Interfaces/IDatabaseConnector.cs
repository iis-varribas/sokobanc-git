﻿using System.Collections.Generic;


namespace DatabaseInterfaces
{
    public interface IDatabaseConnector
    {
        bool UserLogin(string userName, string encodedPassword);
        bool UserRegister(string userName, string firstName, string lastName, string encodedPassword);
        bool UserUnregister(string userName, string encodedPassword);
        int GetUserID(string userName);
        int UserCount();

        int GetLevelSetID(string levelSetName);
        int LevelSetCount();
        string[] GetAllRegisteredLevelSets();
        int GetLevelCount(string levelSetName);

        bool UpdateRelationship(int userID, int levelSetID, int lastCompletedLevel, int totalMovements, int millisSpent);
        List<PlayedLevelSet> GetPlayedSetsForUserID(int userID);
    }
}
