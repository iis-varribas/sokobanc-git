﻿namespace DatabaseInterfaces
{
	[System.Serializable]
    public class PlayedLevelSet
    {
        private int id;
        private string name;
        private int levelCount;
        private int lastCompletedLevel;
        private int totalMovements;
        private int millisSpent;

        public int Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }

        public int LevelCount
        {
            get { return levelCount; }
        }

        public int LastCompletedLevel
        {
            get { return lastCompletedLevel; }
        }

        public int TotalMovements
        {
            get { return totalMovements; }
        }

        public int MillisSpent
        {
            get { return millisSpent; }
        }

        public PlayedLevelSet(int id, string name, int levelCount, int lastCompletedLevel, int totalMovements, int millisSpent)
        {
            this.id = id;
            this.name = name;
            this.levelCount = levelCount;
            this.lastCompletedLevel = lastCompletedLevel;
            this.totalMovements = totalMovements;
            this.millisSpent = millisSpent;
        }
    }
}
