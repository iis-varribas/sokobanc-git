﻿using Sokoban.Library;
using RemotingLifeTimeTools;

namespace Sokoban.GameServer.Interfaces
{
    public interface IGameManager : ISponsable
    {
		LevelSetAbstract GetLevelSet(string name);
    }
}
