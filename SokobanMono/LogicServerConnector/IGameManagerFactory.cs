﻿using System;
using System.Runtime.Remoting.Lifetime;

namespace Sokoban.GameServer.Interfaces
{
    public interface IGameManagerFactory
    {
        IGameManager CreateManager();
    }
}
