using System;

using System.Collections;
using System.Collections.Generic;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Serialization.Formatters;

using Sokoban.MultiPlayerImpl;

using UserDefined.Naming;
using UserDefined.Extensions.Http;

namespace Sokoban.MultiplayerServer
{
	class MainClass
	{
		
		private class PureSingleton : MultiplayerGameImpl
		{
			public override object InitializeLifetimeService()
			{
				return null;
			}
		}
		
		public static void Main (string[] args)
		{
			string SERVICE_NAME = "Multiplayer.rem";
			int SERVICE_PORT = 2222;

			
            // configure a channel
			IDictionary propBag = new Hashtable();
			propBag["port"] = SERVICE_PORT;
			propBag["typeFilterLevel"] = TypeFilterLevel.Full;
			propBag["name"] = string.Format("Channel_{0}:{1}",SERVICE_NAME, SERVICE_PORT);
			
			
#if DEBUG
			string url_base = "http://localhost:"+SERVICE_PORT;
			SoapServerFormatterSinkProvider provider = new SoapServerFormatterSinkProvider();
			provider.TypeFilterLevel = TypeFilterLevel.Full;
			HttpChannel channel = new HttpChannel(propBag, null, provider);
#else
			string url_base = "tcp://localhost:"+SERVICE_PORT;
			BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
			provider.TypeFilterLevel = TypeFilterLevel.Full;
			TcpChannel channel = new TcpChannel(propBag, null, provider);
#endif
            ChannelServices.RegisterChannel(channel, false);
			
			
            // publish the object (lifetime object is forever)
			PureSingleton multiplayerGame = new PureSingleton();
			ObjRef objectPublished = RemotingServices.Marshal(multiplayerGame, SERVICE_NAME);
			Console.WriteLine("uri: "+objectPublished.URI);


			Console.WriteLine("Service is started.");
#if DEBUG
			Console.WriteLine("[HTTP]");
#else
			Console.WriteLine("[TCP]");
#endif
			
			Console.WriteLine("Trying external bind");
			string local_uri = url_base+"/"+SERVICE_NAME;
			string public_uri = ExternalNamingService.ConvertToExternal(local_uri);
			if (public_uri.Contains("193.147.63.47")) public_uri = local_uri; // Patch for develop computer
			bool UsingNamingService = ExternalNamingService.Register(SERVICE_NAME, public_uri);			

			
            Console.WriteLine("Server waiting for requests...");

            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
			
			if (UsingNamingService){
				ExternalNamingService.Deregister(SERVICE_NAME);
			}
			
			// PureSingleton service must be manualmente revoked
			Console.WriteLine("Disconnecting... "+RemotingServices.Disconnect(multiplayerGame));
		}
	}
}

