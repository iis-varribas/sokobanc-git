using System;
using System.Collections;
using System.Threading;
using System.Runtime.Remoting.Lifetime;

using RemotingLifeTimeTools;
using Sokoban.Library;




namespace Sokoban.MultiPlayer
{	
	public delegate void OnMoveEvent(LevelChanges change);
	public delegate void OnFinishEvent(LevelSnapshot instant);
	
	public interface IOpponent : ISponsor
	{		
		string Name
		{
			get;
		}
		
		event OnMoveEvent OnMove;
		event OnFinishEvent OnFinish;
	}
	
	
	
	public class Player : MarshalByRefObjectWithLease, IOpponent, ISponsor, ISponsable
	{		
		private string name;
		
		public Player (string name)
		{
			this.name = name;
			
				lease = (ILease)base.InitializeLifetimeService();
				lease.InitialLeaseTime = TimeSpan.FromSeconds(30);
				lease.RenewOnCallTime = TimeSpan.FromSeconds(30);
		}
		
		public string Name
		{
			get {return name;}
		}
		
		public void SendMove (LevelChanges change)
		{
			if (OnMove == null) return;
			
			//OnMove(change);
			// Subscribed delegates can be from remote objects. Then, a call
			// to a remote method could fail and no more subscribers will be
			// adviced.
			// Therefore is needed to exec manually each delegate in order to
			// catch the exception and continue as normal
			MulticastDelegate m = OnMove as MulticastDelegate;
			foreach (OnMoveEvent d in m.GetInvocationList())
			{
				try{
					d.DynamicInvoke(change);
				}catch(Exception e){
					// When a delegate fail is supposed that object container or 
					// the delegate itself has a problem. Thus it is removed.
					OnMove -= d;
					Console.WriteLine("[Error] SendMove \n<"+e);
				}
			}
		}

		public void SendFinish (LevelSnapshot instant)
		{
			if (OnFinish == null) return;
			
			MulticastDelegate m = OnFinish as MulticastDelegate;
			foreach (OnFinishEvent d in m.GetInvocationList())
			{
				try{
					d.DynamicInvoke(instant);
				}catch(Exception e){
					OnFinish -= d;
					Console.WriteLine("[Error] SendFinish \n<"+e);
				}
			}
		}
		
		public event OnMoveEvent OnMove;
		public event OnFinishEvent OnFinish;
		
		
		public TimeSpan Renewal(ILease lease)
		{
			Console.WriteLine("[Sponsor] (Player) Requested to keep alive "+DateTime.Now);
			return lease.InitialLeaseTime;
		}

	}
	
	[Serializable]
	public abstract class Multiplayer
	{
		
		public abstract LevelAbstract Level
		{
			get;
		}
		
		public abstract Player MySelf
		{
			get;
		}
		
		public abstract IOpponent this[int index]
		{
			get;
		}
		
		public abstract int NrOfOpponents
		{
			get;
		}

	}
	
	
	public abstract class MultiplayerGame : MarshalByRefObject
	{
		
		public abstract Multiplayer Join(Player p);
		
		public abstract LevelAbstract GetMyLevel(Player p, string key, int levelID);
		
		public abstract TimeSpan WaitTime
		{
			get;
		}

	}
}

