#define REMOTE

using System;
using System.Configuration;
using System.Collections;
using System.Threading;

#if REMOTE
using System.Runtime.Remoting;
using UserDefined.Naming;
using Sokoban.GameServer.Interfaces;
#else
using Sokoban.GameServer;
#endif

using RemotingLifeTimeTools;
using Sokoban.MultiPlayer;
using Sokoban.Library;
using Sokoban.API.LevelEvents;



namespace Sokoban.MultiPlayerImpl
{
	[Serializable]
	public class MultiplayerImpl : Multiplayer
	{
		private Player itself;
		private ArrayList opponents;
		private LevelAbstract thelevel;
//		private string key;
		
		
		public MultiplayerImpl(Player itself, IList opponents, LevelAbstract thelevel)
		{
			this.itself = itself;
			this.opponents = new ArrayList(opponents);
			this.thelevel = thelevel;
		}
		
		public override Player MySelf
		{
			get { return itself; }
		}
		
		public override LevelAbstract Level
		{
			get {return thelevel;}
		}
		
		public override IOpponent this[int index]
		{
			get { return (IOpponent) opponents[index]; }
		}
		
		public override int NrOfOpponents
		{
			get { return opponents.Count; }
		}
		
	}
	
	
	public class MultiplayerGameImpl : MultiplayerGame
	{
		private static int MAX_PLAYERS = 5;
		private static TimeSpan PROCESS_TIME_EXECUTION = TimeSpan.FromSeconds(10);
		private static TimeSpan SLEEP_TIME = TimeSpan.FromSeconds(10);
		private static string LEVELSET_CHOSEN = "Calx"; //"boxworld";//"Brainsport";// "AmazingOrimazing";
		
		private readonly Semaphore sem;
		private int count;
		
		private GameContainer game;
		
		
		private readonly Random rand = new Random();
		private readonly BasicSponsor sponsor;
		private IGameManager manager;
		private LevelSetAbstract levelSet;
		
			
		public MultiplayerGameImpl()
		{
			sem = new Semaphore(MAX_PLAYERS, MAX_PLAYERS);
			count = 0;
			
			sponsor = new BasicSponsor();
			
			
			LoadFromConfig();
			ConnectToLevelProvider();
		}
		
		private void ConnectToLevelProvider()
		{
			Console.WriteLine("ConnectToLevelProvider()");

#if REMOTE
			string remoteObjectURI;
			try {
				remoteObjectURI = ExternalNamingService.LookUp("GameManagerFactory.rem");
			}catch(Exception){
				Console.WriteLine("[ERROR] LookUp fail. Trying with hardcored uri");
#if DEBUG
                remoteObjectURI = "http://localhost:2000/GameManagerFactory.rem";
#else
				remoteObjectURI = "tcp://localhost:2000/GameManagerFactory.rem";
#endif
			}
            IGameManagerFactory levelfactory =
				(IGameManagerFactory) Activator.GetObject(
					typeof(IGameManagerFactory),
					remoteObjectURI);

			manager = levelfactory.CreateManager();
			manager.RegisterSponsor(sponsor);

			levelSet = manager.GetLevelSet(LEVELSET_CHOSEN);
			levelSet.RegisterSponsor(sponsor);
#else
			levelSet = new LevelSet();
#endif
			
			Console.WriteLine("Loaded the LevelSet '{0}'. I can serve {1} levels", levelSet.Title, levelSet.NrOfLevels);
		}
		
		private void NewGroup()
		{
			// Check LevelSet
			bool success = false;
			int n = -1;
			for (int i=0; i<5; i++)
			try {
				n = levelSet.NrOfLevels;
				success = true;
				break;
			}catch(RemotingException ){
				ConnectToLevelProvider();
			}

			if (!success)
				throw new Exception("Multiplayer stop service. Level provider is down.");
			
			n = rand.Next(0, n);
			
			LevelAbstract level = levelSet[n];
			level.RegisterSponsor(sponsor);
			game = new GameContainer(level, MAX_PLAYERS);
			
			Console.WriteLine("New group. Chosen level is: "+level.LevelSetName+"/"+level.LevelNr);
		}
		
		
		
		private void CloseGroup()
		{
			Console.WriteLine("Close group.");
			try{game.level.DeregisterSponsor(sponsor);}catch(Exception){}
			game = null;
			
			if (count > 0) sem.Release(count);
			count = 0;
		}
		
		
		
		public override TimeSpan WaitTime
		{
			get { return SLEEP_TIME + PROCESS_TIME_EXECUTION; }
		}
		
		
		

		// IMPORTANT: locks must be taken in the same order. 1:this, 2:mygame
		public override Multiplayer Join(Player player)
		{
			GameContainer mygame = null;
			int local_count = 0;
			
			// There are limited places. Get a ticket for join this group.
			sem.WaitOne();
			
			// Check online state (if player is tired of waiting)
			try{
				Console.WriteLine("Granted access to '{0}'",player.Name);
			}catch(Exception){
				Console.WriteLine("The player goes down. Recovering ticket");
				sem.Release();
				return null;
			}
			// Begin
			try{
			Monitor.Enter(this);
			
			if (count == 0)
			{
				//First Player
				NewGroup();	
			}
			mygame = game;
			Monitor.Enter(mygame);
			mygame.players.Add(player);
			local_count = ++count;
				
			if (count == MAX_PLAYERS)
			{
				// Remove group from public data. 
				// this ensures that mygame will be read only
				CloseGroup();
			}
			Monitor.Exit(this);
			
			if (local_count == MAX_PLAYERS)
			{				
				// Last player must awaken the rest
				Monitor.PulseAll(mygame); //TODO: Migrate to a Semaphore?
				Monitor.Exit(mygame);			
			}else{
				// Wait for joins		
				// [OP 2] Sleep a while. Two options when awake:
				// a. Last player call me
				// b. Wait timeout
				Console.WriteLine("Wait for '{0}'",player.Name);
				Monitor.Wait(mygame, SLEEP_TIME);
				Monitor.Exit(mygame); // I have mygame.lock but I need this.lock for call CloseGroup(). I must release mygame.lock to preserve lock grant order.
				
				// Check awake type
				Monitor.Enter(this);
				Monitor.Enter(mygame);
				if (mygame.players.Count == MAX_PLAYERS){
					// type (a). Do nothing
				}else{
					// type (b). Do the job of last player
					// all of awaken players fight for do this job
					if (mygame == game){
						CloseGroup();
						Monitor.PulseAll(mygame);
					}
				}
				Monitor.Exit(mygame);
				Monitor.Exit(this);
			}

			// Henceforth GameContainer is readonly.
			//  players not need to write in it
			//  nobody else have access to it
			Multiplayer mp;
			if (mygame.players.Count-1 == 0)
			{
				// I'm alone
				mp = null;
			}else{
				// Construct the reply object
				string debug_info = string.Empty;
				ArrayList opponents = new ArrayList();
				debug_info += string.Format("Multiplayer: {0}/{1}\n",mygame.level.LevelSetName, mygame.level.LevelNr);
				foreach (Player p in mygame.players)
				{
					//This works. ArrayList.Remove(player) do not work; hangs.
					if (player == p)
					{
						debug_info += string.Format("  -{0}\n",p.Name);
						continue; 
					}
					debug_info += string.Format("  +{0}\n",p.Name);
					opponents.Add(p);
					p.RegisterSponsor(player);
				}
				Console.Write(debug_info);
				
				// There are a Multiplayer object for each player. Each one of 
				// them must be his own copy of the level (and have different 
				// opponent list.
				LevelAbstract level = (LevelAbstract)mygame.level.Clone();
				level.RegisterSponsor(player);
				mp = new MultiplayerImpl(player, opponents, level);
			}
			
			return mp;
			}finally{
				Monitor.Exit(this);
				if (mygame != null) Monitor.Exit(mygame);
			}
		}
		
		
		// TODO: Make real this functionability.
		// Example:
		//   GetMyLevel(Player p, byte[] crypted_data)
		//   crypted_data: rsa encryption of: ServerUID, PlayerUID, level, levelset, others (opponents, salt, date, etc)
		//     crypted_data as encode64 string for full compatibility?
		//   only Multiplayer Server have private key (autogenerated in startup?)
		//   crypted_data is attached to Multiplayer
		// Corollary: if Multiplayer Server serves game, then it MUST serve level.
		// Against: Multiplayer Server could not be shutted down after serve a game.
		public override LevelAbstract GetMyLevel(Player p, string key, int levelID)
		{
			// Player p and string key will be used for autenticate user
			for (int i=0; i<2; i++)
			try{
				return (LevelAbstract) levelSet[levelID].Clone();
			}catch(RemotingException){
				ConnectToLevelProvider();
			}
			return null;
		}
		
		private class GameContainer
		{
			public ArrayList players;
			public LevelAbstract level;
			
			public GameContainer(LevelAbstract level, int size)
			{
				this.level = level;
				this.players = new ArrayList(size);
			}
		}

		
		private readonly string MAX_PLAYERS_KEY = "Number of Players";
		private readonly string SLEEP_TIME_KEY = "Wait time for opponent joinning";
		private readonly string PROCESS_TIME_EXECUTION_KEY = "Time required for process a Join";
		private readonly string LEVELSET_CHOSEN_KEY = "LevelSet chosen";
		private void LoadFromConfig()
		{
			Console.WriteLine("Setup 'MultiplayerImpl'");
			
			Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			
			Console.WriteLine("  File: "+config.FilePath);
			
			LEVELSET_CHOSEN = config.LoadSetting(LEVELSET_CHOSEN_KEY, LEVELSET_CHOSEN);
			MAX_PLAYERS = int.Parse(config.LoadSetting(MAX_PLAYERS_KEY, MAX_PLAYERS+""));
			SLEEP_TIME = TimeSpan.Parse(config.LoadSetting(SLEEP_TIME_KEY, SLEEP_TIME+""));
			PROCESS_TIME_EXECUTION = TimeSpan.Parse(config.LoadSetting(PROCESS_TIME_EXECUTION_KEY, PROCESS_TIME_EXECUTION+""));
			Console.WriteLine("  Using {0} = {1}", LEVELSET_CHOSEN_KEY, LEVELSET_CHOSEN);
			Console.WriteLine("  Using {0} = {1}", MAX_PLAYERS_KEY, MAX_PLAYERS);
			Console.WriteLine("  Using {0} = {1}", SLEEP_TIME_KEY, SLEEP_TIME);
			Console.WriteLine("  Using {0} = {1}", PROCESS_TIME_EXECUTION_KEY, PROCESS_TIME_EXECUTION);
		}
		
		

	}
	
	
	static partial class ConfigExtensions
	{
		public static string LoadSetting(this Configuration config, string key, string default_value)
		{
			if (config.AppSettings.Settings[key] == null)
			{
				config.AppSettings.Settings.Add(key, default_value);
				config.Save(ConfigurationSaveMode.Modified);
				ConfigurationManager.RefreshSection("appSettings");
			}
			 return config.AppSettings.Settings[key].Value;
		}
	}
}

