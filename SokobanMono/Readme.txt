## Gemeral Info ##
* Code only works in linux
  Tested in (32 and 64 bits. Debian and Ubuntu)
* Console projects might run in Windows with Mono installed, Gtk project might not.
* For non-localhost connection, firewall must be disabled/configured

* SokobanClient doesn't run properly in Mostoles linux computers.
* SokobanClient runs properly in Fuenlabrada linux computers.
  Try remote connection with "ssh -C -Y USER@iotaXX.aulas.gsyc.es"
  Visit bilo.gsyc.es/parte.html


## Project description ##
- SokobanLibrary: prvides basic class/abstractions
- GameServer: provides a levelsets implementation
- LogicServerConnector: inverse dependencies approach for work with GameServer or another levelset implementation (GameServer contract)
- DatabaseInterfaces: contract for Remoting communication with DataBase Service
- MultiplayerServer: implementation of multiplayer game mode
- SokobanAPI: contract for client logic implementation. The only thing that a client (pure UI) must known
- SokobanClientConnector: implementation of SokobanAPI
- SokobanClient: Gtk# UI implementation of sokoban client

- ExternalNamingService: implementation of C# front-end for RESTful naming service
- Extensions: non dependant sokoban code. C# extensions for http and json
- RemotingLifeTimeTools: library for rapid sponsorship development



## Dependencies ##
SokobanClient: Gtk UI. Needs additional dependencies
  + gtk#
  + libglade

For unattended dependencies installation, install monodevelop
  debian based: apt-get install monodevelop



## Executing ##
Project have two connectivity methods:
+ TCP/Binary: fast comunication mode (better data compression and oriented session connexion)
+ Http/Soap: slow comunication mode (worse data compression and Q/A connexion)

For run in Tcp/Binary, compile and execute in Release mode
For run in Http/Soap, compile and execute in Debug mode


For fast usage, there are tree scripts
- rsync.sh: for copy project to testing computers
- cleanner.sh: if MonoDevelop not clean built dir, use this script
- runner.sh: a fast execution of all projects. Combine with ssh -Y if you work with a remote machine


rsync.sh usage:
  Set your own L_DIR, R_DIR, USER and HOSTS

runner.sh usage:
  runner.sh Release|R|r|Debug|D|r

cleanner.sh usage:
  cleanner.sh [ALL]
