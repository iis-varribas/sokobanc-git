using System;
using System.Runtime.Remoting.Lifetime;


namespace RemotingLifeTimeTools
{
	public class BasicSponsor : MarshalByRefObject, ISponsor 
	{
		
		public TimeSpan Renewal(ILease lease)
		{
			Console.WriteLine("[Sponsor] Request to keep alive from "+lease);
			TimeSpan renew_time;
			if (enabled){
				renew_time = lease.InitialLeaseTime;
			}else{
				renew_time = TimeSpan.Zero;
				lease.Unregister(this);
			}
			
			return renew_time;
		}
		
		
		// This is a easy way to stop releases.
		// Is symmetrical since client create it and have access it to 'remove'
		// The good way is to use Unregister, but it requieres access to MarshalRefObject
		// and therefore the knowledgement of his structure (remove object class declaration)
		// We also could execute Unregister code in remote side and send Sponsor as argument.
		// See also System.Runtime.Remoting.Lifetime.ClientSponsor
		private bool enabled = true;
		public bool Enable
		{
			get { return enabled; }
			set { enabled = value; }
		}
	}
}

