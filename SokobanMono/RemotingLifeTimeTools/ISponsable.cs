using System.Runtime.Remoting.Lifetime;
namespace RemotingLifeTimeTools
{
	public interface ISponsable
	{
		
		void RegisterSponsor(ISponsor sponsor);
		
		void DeregisterSponsor(ISponsor sponsor);
		
	}
}

