using System;
using System.Runtime.Remoting.Lifetime;

namespace RemotingLifeTimeTools
{
	public class MarshalByRefObjectWithLease  : MarshalByRefObject, ISponsable
	{
		
        ~MarshalByRefObjectWithLease()
        {
			// When a remote ref object is served, the object is attached to Remoting mechanism and managed port
			// You can make a soft-kill with lease expiration or deattaching the object (lease expiration
			// deattach the object).
			// It is assumed that Base destructor will make a hard-kill of object, but an extra call ensure it
			// and serve to debug purposes (know which objects were alive).
			//ILease lease = (ILease)base.GetLifetimeService();
			string debug_info = string.Empty;
			debug_info += string.Format("{0}\n", this.GetType().ToString());
			if (lease != null)
				debug_info += string.Format("  Lease: State [{0}] Time [{1}]\n", lease.CurrentState, lease.CurrentLeaseTime);
			debug_info += string.Format("  Disconnecting... {0}\n", System.Runtime.Remoting.RemotingServices.Disconnect(this));
			Console.Write(debug_info);
        }
		

		
		// Base call to InitializeLifetimeService() create a new Lease. 
		// System call to InitializeLifetimeService() much more after than object construction.
		// As we need to to Register a Sponsor, we must call to InitializeLifetimeService(),
		// and therefore, there will be two calls to this method and thwo Lease objects. Second
		// object replace the first.
		// So we must make a singleton/cached Lease to it works.
		protected ILease lease;
        public override object InitializeLifetimeService()
        {
            // truco para que el objeto viva para siempre
			// que no me vigile nadie para matarme
			// Muy malo porque el objeto nunca será destruido
			// hasta que se reinicie el ordenador
            // return null;    
			
			if (lease == null) lease = (ILease) base.InitializeLifetimeService();
    		if (lease.CurrentState == LeaseState.Initial)
			{
				
				lease.InitialLeaseTime = TimeSpan.FromMinutes(10);
				lease.SponsorshipTimeout = TimeSpan.FromSeconds(90);
				lease.RenewOnCallTime = TimeSpan.FromSeconds(10);
			}
			
//			Console.WriteLine("Lease: State [{0}] Time [{1}]", lease.CurrentState, lease.CurrentLeaseTime);
			
    		return lease;
        }
		
		// For fast Sponsor register
		public void RegisterSponsor(ISponsor sponsor)
		{
			if (lease == null) this.InitializeLifetimeService();
			lease.Register(sponsor);
		}
		
		public void DeregisterSponsor(ISponsor sponsor)
		{
			if (lease == null) return;
			lease.Unregister(sponsor);
		}

	}

}

