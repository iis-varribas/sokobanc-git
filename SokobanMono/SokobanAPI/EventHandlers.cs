using System;
using Sokoban.Library;

namespace Sokoban.API
{
	namespace LevelEvents
	{
		public delegate void OnLoadHandler(LevelDescription level);
		public delegate void OnChangeHandler(LevelChanges change);
		public delegate void OnFinishHandler(LevelDescription level);
		
		public class LevelHandler : MarshalByRefObject
		{
			public OnLoadHandler OLoad;
			public OnChangeHandler OnChange;
			public OnFinishHandler OnFinish;
			
			public void SendLevel(LevelDescription level)
			{
				if (OLoad != null) OLoad(level);
			}
			
			public void SendChanges(LevelChanges changes)
			{
				if (OnChange != null) OnChange(changes);
			}
			
			public void SendFinishSignal(LevelDescription level)
			{
				if (OnFinish != null) OnFinish(level);
			}
			
			
		}
	}
	
	namespace LevelSetEvents
	{
		public delegate void OnLoadHandler(LevelSetDescription levelset);
	}
	
	
	public abstract class LevelEventsHandler : MarshalByRefObject
	{
		protected abstract void OnLoad(LevelDescription level);
		protected abstract void OnChange(LevelChanges change);
		protected abstract void OnFinish(LevelDescription level);
		
	}
}

