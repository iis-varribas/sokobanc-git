using System;
using Sokoban.Library;

namespace Sokoban.API
{
	public interface ICurrentLevel : ILevelProperties
	{
		
		#region Methods
		
		void ChangeLevel(int level_number);
		void NextLevel();
		
		#endregion
		
		
		#region Properties
		LevelDescription LevelDescription
		{
			get;
		}
		
		LevelSnapshot GetInstant();
		LevelChanges GetChanges();
		
		#endregion


		#region Events
		
		event LevelEvents.OnLoadHandler OnLoad;
		event LevelEvents.OnChangeHandler OnChange;
		event LevelEvents.OnFinishHandler OnFinish;

		
		#endregion
	}
}

