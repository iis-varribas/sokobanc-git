using Sokoban.Library;

namespace Sokoban.API
{
	public interface ICurrentLevelSet :ILevelSetProperties
	{
		
		#region Methods
		
		void ChangeLevelSet(string levelset_name);
		
		#endregion
		
		
		#region Properties
		
		LevelSetDescription LevelSetDescription
		{
			get;
		}
		LevelDescription[] Levels
		{
			get;
		}
		
		#endregion

		
		#region Events

		event LevelSetEvents.OnLoadHandler OnLoad;

		#endregion
	}
}

