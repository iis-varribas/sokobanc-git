namespace Sokoban.API
{
	public interface ICurrentUser : IUser, IUserManager
	{

		
		string LastLevelSetPlayed{
			get;
		}

		
	}
}

