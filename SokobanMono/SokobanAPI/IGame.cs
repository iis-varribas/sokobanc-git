namespace Sokoban.API
{
	public partial interface IGame
	{
		
		ICurrentUser User
		{
			get;
		}
			
		ICurrentLevel CurrentLevel
		{
			get;
		}
		
		ICurrentLevelSet CurrentLevelSet
		{
			get;
		}
		
		IMovementExtra Move
		{
			get;
		}
		
		
		void Terminate();
		
	}
}

