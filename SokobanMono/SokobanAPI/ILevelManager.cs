namespace Sokoban.API
{
	public interface ILevelManager
	{
		
		#region Methods
		
		void ChangeLevel(int level_number);
		
		#endregion

	}
}

