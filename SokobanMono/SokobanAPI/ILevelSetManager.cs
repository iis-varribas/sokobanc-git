namespace Sokoban.API
{
	public interface ILevelSetManager
	{
		
		#region Methods
		
		void ChangeLevelSet(string levelset_name);
		
		#endregion

	}
}

