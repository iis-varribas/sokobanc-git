using System;
namespace Sokoban.API
{
	public interface IMovement
	{
		
		void Up();
		void Down();
		void Left();
		void Right();
	}
}

