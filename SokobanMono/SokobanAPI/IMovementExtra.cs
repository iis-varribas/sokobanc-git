using System;
namespace Sokoban.API
{
	public interface IMovementExtra : IMovement
	{
		void Reset();
	}
}

