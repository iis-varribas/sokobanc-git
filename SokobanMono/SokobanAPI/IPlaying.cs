namespace Sokoban.API
{
	public interface IPlaying
	{
		
		int CurrentLevel
		{
			get;
		}
		
		string CurrentLevelSet
		{
			get;
		}
		
		
		void LoadLevelSet();
		
		void LoadLevel();
		
		void Move();
	}
}

