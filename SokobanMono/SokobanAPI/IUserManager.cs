namespace Sokoban.API
{
	public interface IUserManager
	{
		bool Login(string user, string passwd);
		bool Register(string user, string passwd);
		
	}
}

