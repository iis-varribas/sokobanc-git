using System;
using System.Runtime.Serialization;

namespace SokobanAPI
{
	[Serializable]
	public class IllegalAccessException : System.Exception
	{
		public IllegalAccessException(string member) : base (member)
		{
		}
		
		
		
	protected IllegalAccessException(SerializationInfo info, StreamingContext context) : base(info, context) 
	{
	}
	
	//[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter = true)]
	public override void GetObjectData(SerializationInfo info, StreamingContext context) 
	{
		base.GetObjectData(info, context);
	}

	
	
	}

}

