using System;
using System.Drawing;
using Sokoban.Library;

namespace SokobanClient
{
	
	
	
	public class GraficalLevel
	{
		private Image ImgSpace;
		private Bitmap levelImage;
		private Graphics g;
		private float scale;
		
		private int levelWidth;
		private int levelHeight;
		
		private int width;
		private int height;

		
		public GraficalLevel (int _width, int _height, float scale)
		{			
			this.scale = scale;
			this.levelWidth  = _width;
			this.levelHeight = _height;
			this.ImgSpace = GetImage(ItemType.Space, MoveDirection.Right);
			
			InitializeLevelImage();
		}
		
		public GraficalLevel (int _width, int _height) : this (_width, _height, 1)
		{
		}

		private void InitializeLevelImage()
		{
			this.width  = (levelWidth  + 2) * ImageSize;
            this.height = (levelHeight + 2) * ImageSize;

            this.levelImage = new Bitmap(width, height);
			this.g = Graphics.FromImage(levelImage);
			
			this.drawBoard();
		}
		
		
		//abstract protected Image GetImage(ItemType itemType, MoveDirection direction);
		protected Image GetImage(ItemType itemType, MoveDirection direction)
		{
			return LevelImages.GetLevelImage(itemType, direction);
		}
		
		protected int ImageSize
		{
			get { return (int) (LevelImages.ITEM_SIZE * scale); }
		}
		
		public float Scale{
			get { return scale; }
			set 
			{
				this.scale = value;
				InitializeLevelImage();
			}
		}
		
		
		
		#region Properties
		public Image Image
		{
			get { return levelImage; }
		}
		
		public int Width
		{
			get{ return width; }
		}
		
		public int Height
		{
			get { return height; }
		}
		
		#endregion
		
		
		

		
		
		/// <summary>
        /// This method draws the level on screen. Around the level there are
        /// extra rows and columns to make it look better. The first 3 for-
        /// statements draw this border. Then we load the level map and step
        /// through it line by line, and character by character. Depending on
        /// the ItemType in the level map, we draw the corresponding image.
        /// </summary>
        /// <returns>The 'level' image that will be drawn to screen</returns>
		private void drawBoard()
		{					 
            // Draw the border around the level
            for (int i = 0; i < levelWidth + 2; i++)
            {
                g.DrawImage(ImgSpace, ImageSize * i, 0,
                    ImageSize, ImageSize);
                g.DrawImage(ImgSpace, ImageSize * i,
                    (levelHeight + 1) * ImageSize, ImageSize, ImageSize);
            }
            for (int i = 1; i < levelHeight + 1; i++)
                g.DrawImage(ImgSpace, 0, ImageSize * i,
                    ImageSize, ImageSize);
            for (int i = 1; i < levelHeight + 1; i++)
                g.DrawImage(ImgSpace, (levelWidth + 1) * ImageSize,
                    ImageSize * i, ImageSize, ImageSize);
		}
		
		
		public void drawLevel(LevelSnapshot instant)
		{			            
			foreach (Item cell in instant.cells)
			{
				Image image = GetImage(cell.ItemType, instant.direction);
				g.DrawImage(image, 
				            ImageSize + cell.XPos * ImageSize,
                        	ImageSize + cell.YPos * ImageSize, 
				            ImageSize, ImageSize);
			}
		}
		
		public void drawChanges(LevelChanges update)
		{			
			foreach (Item change in update.changes)
			{
				Image image = GetImage(change.ItemType, update.direction);
				g.DrawImage(image, 
				            ImageSize + change.XPos * ImageSize,
                        	ImageSize + change.YPos * ImageSize, 
				            ImageSize, ImageSize);
			}
		}

	}
	
	
	
	
}

