// For fast switch between local or remote execution
// Using DEGUB for HTTP and Release for TCP
//#define REMOTE

using System;


using Gdk;
using Gtk;
using Glade;


namespace SokobanClient.GtkUI
{
	
	public class AboutGladeLogic
	{
		// Main window:
		[Widget] 
		private Gtk.AboutDialog About;
		
	
		public AboutGladeLogic() 
		{
			// Create the main window
			Glade.XML gxml = new Glade.XML("SokobanClient.Gtk.About.glade", "About");
			gxml.Autoconnect (this);
			About.Show();
		}
		
	}

}