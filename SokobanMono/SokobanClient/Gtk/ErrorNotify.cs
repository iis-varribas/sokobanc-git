using System;
using System.Collections;
using Gtk;

namespace SokobanClient.GtkUI
{
	public static class ErrorNotify
	{
		
		static ErrorNotify()
		{
			GLib.ExceptionManager.UnhandledException += UnhandledToHandler;
		}
		
		
		public static void ModalMessageDialog(Window parent, string title, string message)
		{
				MessageDialog msdSame = new MessageDialog(parent, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, message);
	   			msdSame.Title=title;
				msdSame.Run();
				msdSame.Destroy();
		}
		
		private static bool _WriteUnhandledInConsole = false;
		public static bool WriteUnhandledInConsole
		{
			get { return _WriteUnhandledInConsole; }
			set 
			{ 
				if (value == _WriteUnhandledInConsole) return;
				if (value){
					GLib.ExceptionManager.UnhandledException += WriteUnhandledInConsoleHandler;
				}else{
					GLib.ExceptionManager.UnhandledException -= WriteUnhandledInConsoleHandler;
				}
				_WriteUnhandledInConsole = value;
			}
		}
		
		public static void WriteUnhandledInConsoleHandler(GLib.UnhandledExceptionArgs args)
		{
			Exception e = ((Exception)args.ExceptionObject);
			if (e is System.Reflection.TargetInvocationException && e.InnerException != null)
				e = e.InnerException;
			Console.WriteLine("======================");
			Console.WriteLine( e.Message );
			Console.WriteLine("----------------------");
			Console.WriteLine(e);
			args.ExitApplication = false;
		}
		
		
		
		private static ArrayList handlerstack = new ArrayList();
		public static void Put(IUnhadledExceptionHandler handler)
		{
			handlerstack.Insert(0, handler);
		}
		
		
		public static void Quit(IUnhadledExceptionHandler handler)
		{
			handlerstack.Remove(handler);
		}
		private static void UnhandledToHandler(GLib.UnhandledExceptionArgs args)
		{
			Exception e = ((Exception)args.ExceptionObject);
			if (e is System.Reflection.TargetInvocationException && e.InnerException != null)
				e = e.InnerException;
			foreach ( IUnhadledExceptionHandler handler in handlerstack)
			{
				if (handler.HandleException(e))
				{
					args.ExitApplication = false;
					break;
				}
			}
			
		}
	}
	
	
	public interface IUnhadledExceptionHandler
	{
		bool HandleException(Exception exception);
	}
}

