namespace SokobanClient.GtkUI
{
	public interface IWindowGladeConnector
	{
		#region Properties
		
		Gtk.Window Window
		{
			get;
		}
		
		#endregion
	}
}

