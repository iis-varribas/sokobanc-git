// For fast switch between local or remote execution
// Using DEGUB for HTTP and Release for TCP
//#define REMOTE

using System;


using Gdk;
using Gtk;
using Glade;


namespace SokobanClient.GtkUI
{
	
	public class LoginGladeLogic : IWindowGladeConnector
	{
		// Main window:
		[Widget] 
		private Gtk.Dialog dialog1;
		

		// Text area
		[Widget] 
		private Gtk.Entry entry_user;
		
		[Widget] 
		private Gtk.Entry entry_pass;
		
		

	
		public LoginGladeLogic() 
		{
			// Create the main window
			Glade.XML gxml = new Glade.XML("SokobanClient.Gtk.Login.glade", "dialog1");
			gxml.Autoconnect (this);
			dialog1.Show();
			entry_user.Text = "a";
			entry_pass.Text = "a";
		}
		
		public Gtk.Window Window
		{ 
			get {return dialog1;}
		}


		#region EventHandlers
		
		private void Window_OnClose (object sender, EventArgs e)
		{
			MyGame.Exit();
		}
		
		private void Window_OnDeleteEvent(object o, DeleteEventArgs args) 
		{
			MyGame.Instance.Terminate();
			Application.Quit();
			args.RetVal = true;
		}
		
		private void Login_OnPressedEvent (object sender, EventArgs e)
		{
			if (MyGame.Instance.User.Login(entry_user.Text, entry_pass.Text))
			{
				dialog1.Hide();
				dialog1.Destroy();
				dialog1 = null;
//				new MainWindowGladeLogic();
//				string levelset = MyGame.Instance.User.LastLevelSetPlayed;
//				MyGame.Instance.CurrentLevelSet.ChangeLevelSet(levelset);
				new SelectorLevelSetGladeLogic();
			}
			else
			{
				Console.WriteLine("Fallo al loguearse, compruebe su usuario y contraseña");
			}
		}
		
		private void Registro_OnPressedEvent (object sender, EventArgs e)
		{
			if (MyGame.Instance.User.Register(entry_user.Text, entry_pass.Text))
			{
				Console.WriteLine("Registro correcto. Logueese");
				
			}
			else
			{
				Console.WriteLine("El usuario ya existe");
			}
		}
		
		#endregion
	}

}