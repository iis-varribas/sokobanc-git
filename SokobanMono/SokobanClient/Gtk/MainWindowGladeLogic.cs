using System;
using System.Timers;
using System.Diagnostics;
using System.Drawing;
using System.IO;

using Gdk;
using Gtk;
using Glade;


using Sokoban.ClientConnector;
using Sokoban.Library;


namespace SokobanClient.GtkUI
{
	
	public class MainWindowGladeLogic : MarshalByRefObject, IWindowGladeConnector, IUnhadledExceptionHandler
	{		
		// Window
		[Widget] 
		private Gtk.Window MainWindow;
		
		// Paintable (for level) area:
		[Widget] 
		private Gtk.DrawingArea drawArea;
		private GtkLevelDrawer levelDrawer;
		
		// Status Bar
		[Widget] 
		private Gtk.Statusbar statusbar;
		private Gtk.Label sb_moves;
		private Gtk.Label sb_time;
		private Timer timer;
		private Stopwatch stopwatch;

		
		private Boolean moveIsDisabled = true;		
		
		
		public MainWindowGladeLogic()
		{
			// Create the main window
			Glade.XML gxml = new Glade.XML("GTK/MainWindow.glade", "MainWindow");
			gxml.Autoconnect(this);	
			
			sb_moves = new Label();
			statusbar.Add(sb_moves);
			sb_time = new Label();
			statusbar.Add(sb_time);
			statusbar.ShowAll();
			
			stopwatch = new Stopwatch();
			timer = new Timer();
			timer.Elapsed += delegate 
			{ 
				TimeSpan ts = stopwatch.Elapsed;
				sb_time.Text = string.Format("{0} seconds", (int)ts.TotalSeconds);
			};
            timer.Interval = (1000) * (1);              // Timer will tick evert second
            timer.Enabled = false;                       // Enable the timer
            //timer.Start();                              // Start the timer
			
			
			ErrorNotify.Put(this);		

			//Init game
			InitializeGame();
		}
		
		~MainWindowGladeLogic()
		{
			ErrorNotify.Quit(this);
		}
		
		public Gtk.Window Window
		{
			get { return MainWindow;}
		}
		
		
		private void DrawArea_OnExposeEvent (object o, ExposeEventArgs args)
		{		
//			//Console.WriteLine("SokobanBoard.OnDrawingAreaExpose");
//			PaintLevelPicture();
//			// return true because we've handled this event, so no
//			// further processing is required.
//			args.RetVal = true;
		}
		
		
		private void Window_OnDeleteEvent(object o, DeleteEventArgs args) 
		{
			MyGame.Exit();
			args.RetVal = true;
		}
		
		
		public void DrawArea_OnKeyPressEvent(object o, KeyPressEventArgs args)
		{
			if ( LevelOptionsKeysHandler(o, args) ) return;
			if ( moveIsDisabled ) return;
			if ( MovementLogicKeysHandler(o, args) ) return;
			if ( AdvancedMoveKeysHandler(o, args) ) return;
			
		}
	
		private bool MovementLogicKeysHandler(object o, KeyPressEventArgs args)
		{
			bool handled = true;
			switch (args.Event.Key)
		    {
		        case Gdk.Key.Up:
					MyGame.Instance.Move.Up();
			        break;
                case Gdk.Key.Down:
                    MyGame.Instance.Move.Down();
                    break;
                case Gdk.Key.Right:
                    MyGame.Instance.Move.Right();
                    break;
                case Gdk.Key.Left:
                    MyGame.Instance.Move.Left();
                    break;
				default:
					handled = false;
					break;
		    }
			return handled;

		}
		
		private bool LevelOptionsKeysHandler(object o, KeyPressEventArgs args)
		{
			bool handled = true;
			switch (args.Event.Key)
		    {
		        case Gdk.Key.R:
				case Gdk.Key.r:
					if ( (args.Event.State & Gdk.ModifierType.ControlMask) != 0){
						Menu_Level_Reset(o, args);
					}
				break;
				default:
					handled = false;
					break;
		    }
			return handled;
		}
		
		private bool AdvancedMoveKeysHandler(object o, KeyPressEventArgs args)
		{
			bool handled = true;
			switch (args.Event.Key)
		    {
		        case Gdk.Key.R:
				case Gdk.Key.r:
		            	Menu_Level_Redo(o, args);
		            break;
                case Gdk.Key.U:
				case Gdk.Key.u:
                    Menu_Level_Undo(o, args);
                    break;
				default:
					handled = false;
					break;
		    }
			return handled;
		}
		
		
		private void Menu_Quit (object sender, EventArgs e)
		{
			MyGame.Exit();
		}
		
		private void Menu_ChangeUser (object sender, EventArgs e)
		{
//			MainWindow.Hide();
			new LoginGladeLogic();
//			MainWindow.Show();
		}
		
		private void Menu_Multiplayer (object sender, EventArgs e)
		{
//			MainWindow.Hide();
			new MultiplayerGladeLogic();
//			MainWindow.Show();
		}
		
		private void Menu_LevelSet_Info (object sender, EventArgs e)
		{
			LevelSetDescription desc = MyGame.Instance.CurrentLevelSet.LevelSetDescription;
			string info = string.Format(
			                            "Title: {0}\n" +
										"Levels: {1}\n" +
			                            "Author: {2}",
			                            desc.Title, desc.NrOfLevelsInSet, desc.Author);
			MessageDialog msdSame = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, info);
			msdSame.Title="LevelSet description";
			msdSame.Run();
			msdSame.Destroy();
		}
		
		private void Menu_Level_Info (object sender, EventArgs e)
		{
			LevelDescription desc = MyGame.Instance.CurrentLevel.LevelDescription;
			string info = string.Format(
			                            "Name: {0}\n" +
			                            "From: {1}\n" +
										"Boxes: {2}\n"+
			                            "Dimensions: {3}",
			                            desc.Name, 
			                            desc.LevelSetName+"/"+desc.LevelNr,
			                            desc.NrOfGoals,
			                            desc.Width+"x"+desc.Height);
			MessageDialog msdSame = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, info);
			msdSame.Title="Level description";
			msdSame.Run();
			msdSame.Destroy();
		}
		
		private void Menu_ElegirSerie (object sender, EventArgs e)
		{
			new SelectorLevelSetGladeLogic();
		}
		
		private void Menu_ElegirNivel (object sender, EventArgs e)
		{
			new SelectorLevelGladeLogic();
		}
		
		private void Menu_Level_Reset (object sender, EventArgs e)
		{
			Console.WriteLine("Reset level");
			MyGame.Instance.Move.Reset();
			moveIsDisabled = false;
		}
		
		private void Menu_Level_Undo (object sender, EventArgs e)
		{
			Console.WriteLine("Undo");
		}
		
		private void Menu_Level_Redo (object sender, EventArgs e)
		{
			Console.WriteLine("Redo");
		}
		
		
		private void Menu_About (object sender, EventArgs e)
		{
			new AboutGladeLogic();
		}
		
		private void Level_OnChanges(LevelChanges changes)
		{
			Gtk.Application.Invoke(delegate 
			{
				levelDrawer.PutChanges(changes);
			
				sb_moves.Text = changes.moves+" moves";
			});
		}
		
		private void Level_OnFinish(LevelDescription level)
		{
			Gtk.Application.Invoke(delegate 
			{
				string levelName = MyGame.Instance.CurrentLevel.Name;
				int levelMoves = MyGame.Instance.CurrentLevel.GetInstant().moves;
				string message = string.Format("Level '{0}' finished.\n You did {1} moves.\n Time elapsed: {2}",
				                               levelName,
				                               levelMoves,
				                               stopwatch.Elapsed);
				Console.WriteLine(message);
				moveIsDisabled = true;
				timer.Stop();
				stopwatch.Stop();
				MessageDialog msdSame = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, message+"\n Go to Next Level?");
	   			msdSame.Title="Level Finished";
				ResponseType reply = (ResponseType)msdSame.Run();
				msdSame.Destroy();
				Console.WriteLine(reply);
				if (reply == ResponseType.Yes){
					MyGame.Instance.CurrentLevel.NextLevel();
				}
				
				sb_moves.Text = string.Empty;
				sb_time.Text = string.Empty;
			});
		}
		
		private void Level_OnCreate(LevelDescription level)
		{
			Gtk.Application.Invoke(delegate 
			{
				levelDrawer.PutLevel(MyGame.Instance.CurrentLevel.GetInstant(), level.Width, level.Height);
				statusbar.Push(0, level.LevelSetName+" / "+level.Name);
				moveIsDisabled = false;
				stopwatch.Reset();
				stopwatch.Start();
				timer.Start();
			});
		}
		
 
	
		
		/// <summary>
	    /// Sets the data for PlayerData, LevelSet, etc..
	    /// </summary>
	    private void InitializeGame()
	    {
			levelDrawer = new GtkLevelDrawer(drawArea, 1);
			
			MyGame.Instance.CurrentLevel.OnLoad += Level_OnCreate;
			MyGame.Instance.CurrentLevel.OnChange += Level_OnChanges;
			MyGame.Instance.CurrentLevel.OnFinish += Level_OnFinish;
		}
		
		
		public bool HandleException(Exception e)
		{
			if (e is System.Runtime.Remoting.RemotingException) {
				// Level, LevelSet or GameManager could be down. Relogin for restart it
				MessageDialog msdSame = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Error, ButtonsType.YesNo, "Gamming service is down. You can try to login again to prove if it is now solved.");
   				msdSame.Title="Error";
				ResponseType reply = (ResponseType) msdSame.Run();
				msdSame.Destroy();
				
				if (reply == ResponseType.Yes){
					new LoginGladeLogic();
				}
			} else if (e is System.Runtime.Serialization.SerializationException) {
				if (e.Message.Equals("Could not find type 'System.Collections.ListDictionaryInternal'")){
					Console.WriteLine("Could not find type 'System.Collections.ListDictionaryInternal'");
				}
				ErrorNotify.ModalMessageDialog(MainWindow, "ListDictionaryInternal", "Imposible conectarse a la base de datos");
			} else if (e is System.Net.WebException) {
				ErrorNotify.ModalMessageDialog(MainWindow, "Error de Red", "Parece que hay un problema de comunicación de red. Inténtelo más tarde");
			} else {
				ErrorNotify.ModalMessageDialog(MainWindow, "Error", e.Message);
			}
			return true;
		}

	}
	


}