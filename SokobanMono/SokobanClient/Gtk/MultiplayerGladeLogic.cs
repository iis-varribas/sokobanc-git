//#define TESTING

using System;
using System.Drawing;
using System.IO;

using Gdk;
using Gtk;
using Glade;

using System.Collections;
using System.Runtime.Remoting;

using Sokoban.ClientConnector;
using Sokoban.Library;
using Sokoban.MultiPlayer;

using UserDefined.Naming;


namespace SokobanClient.GtkUI
{
	
	public class MultiplayerGladeLogic : MarshalByRefObject, IWindowGladeConnector, IUnhadledExceptionHandler
	{		
		// Window
		[Widget] 
		internal Gtk.Window MainWindow;
		
		// Paintable (for level) area:
		[Widget] 
		private Gtk.DrawingArea drawArea;
		
		// Level Drawer (from internal representation to simple image)
		private GtkLevelDrawer levelDrawer;
		
		// Status Bar
		[Widget] 
		private Gtk.Statusbar statusbar;
		private Gtk.Label sb_moves;


		//Multiplayer Options
		private Player player;
		private LevelAbstract theLevel;
		internal Boolean moveIsDisabled;
		
		private Opponent[] opponents;
		private GtkLevelDrawer[] lds;
		private Gtk.DrawingArea[] drawAreas;
		
		//Auxiliar
//		[Widget] private Gtk.Table table;
		[Widget] private Gtk.DrawingArea drawArea1;
		[Widget] private Gtk.DrawingArea drawArea2;
		[Widget] private Gtk.DrawingArea drawArea3;
		[Widget] private Gtk.DrawingArea drawArea4;
		
		
		
		private LevelDescription levelDescription;

		
		public MultiplayerGladeLogic()
		{
			// Create the main window
			Glade.XML gxml = new Glade.XML("GTK/Multiplayer.glade", "MainWindow");
			gxml.Autoconnect(this);
			
			drawAreas = new Gtk.DrawingArea[]{drawArea1, drawArea2, drawArea3, drawArea4};
			foreach (Gtk.DrawingArea da in drawAreas)
				da.Hide();
	
				
			sb_moves = new Label();
			sb_moves.Show();
			statusbar.Add(sb_moves);

			ErrorNotify.Put(this);

			Gtk.Application.RunIteration();
			//Init game
			InitializeGame();
		}
		
		~MultiplayerGladeLogic()
		{
			EndGame();
		}
		
		public Gtk.Window Window
		{
			get { return MainWindow;}
		}
		
		
		private void DrawArea_OnExposeEvent (object o, ExposeEventArgs args)
		{		
//			//Console.WriteLine("SokobanBoard.OnDrawingAreaExpose");
//			PaintLevelPicture();
//			// return true because we've handled this event, so no
//			// further processing is required.
//			args.RetVal = true;
		}
		
		
		private void Window_OnDeleteEvent(object o, DeleteEventArgs args) 
		{
			MainWindow.HideAll();
			MainWindow.Dispose();
			EndGame();
			args.RetVal = true;
		}
		
		
		public void DrawArea_OnKeyPressEvent(object o, KeyPressEventArgs args)
		{
			if ( moveIsDisabled ) return;
			try {
				if ( MovementLogicKeysHandler(o, args) ) return;
				if ( LevelOptionsKeysHandler(o, args) ) return;
				
			}catch(System.Net.WebException e) {
				MessageDialog msdSame = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, "Parece que hay un problema de comunicación de red. Inténtelo más tarde");
	   			msdSame.Title=e.Message;
				msdSame.Run();
				msdSame.Destroy();
			}catch(RemotingException){
				MessageDialog msdSame = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Error, ButtonsType.YesNo, "Level Server Provider has lose your level. You want to retry level? (or exit).");
   				msdSame.Title="Error";
				ResponseType reply = (ResponseType) msdSame.Run();
				msdSame.Destroy();
				if (reply == ResponseType.Yes) {
					MultiplayerGame multiplayergame = ConnectToMultiplayerService();
					theLevel = multiplayergame.GetMyLevel(null, "unused", levelDescription.LevelNr);
					levelDrawer.PutLevel(theLevel.GetInstant(), levelDescription.Width, levelDescription.Height);
					
				} else {
					EndGame();
				}
			}
		}
	
		private bool MovementLogicKeysHandler(object o, KeyPressEventArgs args)
		{
			bool handled = true;
			bool moved = false;
			switch (args.Event.Key)
		    {
		        case Gdk.Key.Up:
					moved = theLevel.MoveSokoban(MoveDirection.Up);
			        break;
                case Gdk.Key.Down:
                    moved = theLevel.MoveSokoban(MoveDirection.Down);
                    break;
                case Gdk.Key.Right:
                    moved = theLevel.MoveSokoban(MoveDirection.Right);
                    break;
                case Gdk.Key.Left:
                    moved = theLevel.MoveSokoban(MoveDirection.Left);
                    break;
				default:
					handled = false;
					break;
		    }
			if (moved)
			{
				Level_OnMove(theLevel.GetChanges());
				if (theLevel.IsFinished())
					Level_OnFinish(theLevel.GetDescription(), theLevel.GetInstant());
			}
			return handled;

		}
		
		private bool LevelOptionsKeysHandler(object o, KeyPressEventArgs args)
		{
			bool handled = true;
			switch (args.Event.Key)
		    {
		        case Gdk.Key.R:
				case Gdk.Key.r:
					if ( (args.Event.State & Gdk.ModifierType.ControlMask) != 0)
					{
						theLevel.ResetLevel();
						Level_OnMove(theLevel.GetChanges());
					}
		            break;
				default:
					handled = false;
					break;
		    }
			return handled;
		}
		


		
		private void Level_OnMove(LevelChanges changes)
		{
			levelDrawer.PutChanges(changes);
			sb_moves.Text = changes.moves+" moves";
			player.SendMove(changes);
		}
		
		
		private void Level_OnFinish(LevelDescription level, LevelSnapshot instant)
		{
			Console.WriteLine("Level Finished");
			moveIsDisabled = true;
			player.SendFinish(instant);
			

			MessageDialog msdSame = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "Level "+level.Name+" finished in "+instant.moves+" moves.");
   			msdSame.Title="Level Finished";
			msdSame.Run();			
			msdSame.Destroy();

			
			EndGame();

		}
		
		private void EndGame()
		{
			try {
				if (opponents != null){
					foreach (Opponent op in opponents){
						op.Close();
					}
					opponents = null;
				}
			}catch(Exception){}
			try {
				if (theLevel != null){
					theLevel = null;
					Console.WriteLine("Unpluging player... "+RemotingServices.Disconnect(player));
				}
			}catch(Exception){}
			
			if (MainWindow != null) {
				MainWindow.Destroy();
				MainWindow = null;
			}
			
			ErrorNotify.Quit(this);
		}

		
		/// <summary>
	    /// Sets the data for PlayerData, LevelSet, etc..
	    /// </summary>
	    private void InitializeGame()
	    {
			string player_name;
			try{
				player_name = MyGame.Instance.User.Name;
			}catch(Exception){
				player_name = string.Format("guest_{0}{1}", DateTime.Now.Second, DateTime.Now.Millisecond);
			}
			MainWindow.Title = "Multiplayer mode: "+player_name;

			
            MultiplayerGame multiplayergame = ConnectToMultiplayerService();
			System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseTime = multiplayergame.WaitTime;
			player = new Player(player_name);
			
			Multiplayer mp = multiplayergame.Join(player);
			if (mp == null)
			{
				MessageDialog msdSame = new MessageDialog
					(MainWindow, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "The aren't opponents to battle. Try later or call you friends.")
					{Title="No players"};
				msdSame.Run();
				msdSame.Destroy();
				EndGame();
				return;
			}

			theLevel = mp.Level;
			
			levelDescription = theLevel.GetDescription();
			LevelSnapshot leveli = theLevel.GetInstant();

			lds = new GtkLevelDrawer[mp.NrOfOpponents];
			opponents = new Opponent[lds.Length];
			
			Console.WriteLine("Myself: "+player.Name);
			for (int i=0; i<opponents.Length; i++)
			{
				lds[i] = new GtkLevelDrawer(drawAreas[i], 0.5f);
				lds[i].PutLevel(leveli, levelDescription.Width, levelDescription.Height);
				opponents[i] = new Opponent(this, mp[i], lds[i]);
				drawAreas[i].Show();
				Console.WriteLine("  vs: "+mp[i].Name);
				player.RegisterSponsor(mp[i]);
			}
			
			
			levelDrawer = new GtkLevelDrawer(drawArea, 1);
			levelDrawer.PutLevel(leveli, levelDescription.Width, levelDescription.Height);
			
			statusbar.Push(0, theLevel.LevelSetName+" / "+theLevel.Name);
			
			MyGame.OnExit += delegate {
				EndGame();	
			};
		}
		
		
		private MultiplayerGame ConnectToMultiplayerService()
		{
			string remoteObjectURI;
			try {
				remoteObjectURI = ExternalNamingService.LookUp("Multiplayer.rem");
			}catch(Exception){
				Console.WriteLine("[ERROR] LookUp fail. Trying with hardcored uri");
#if DEBUG
                remoteObjectURI = "http://localhost:2222/Multiplayer.rem";
#else
				remoteObjectURI = "tcp://localhost:2222/Multiplayer.rem";
#endif
			}
			MultiplayerGame multiplayergame = (MultiplayerGame) Activator.GetObject(
				typeof(MultiplayerGame),
				remoteObjectURI);
			return multiplayergame;
		}
		
			
		private class Opponent : MarshalByRefObject
		{
			private GtkLevelDrawer levelDrawer;
			private MultiplayerGladeLogic super;
			private IOpponent opponent;

			
			public Opponent(MultiplayerGladeLogic that, IOpponent opp, GtkLevelDrawer ld)
			{
				super = that;
				levelDrawer = ld;
				opponent = opp;
				opponent.OnMove += this.OnMove;
				opponent.OnFinish += this.OnFinish;
			}
			
			public void Close()
			{
				try{
					opponent.OnMove -= this.OnMove;
					opponent.OnFinish -= this.OnFinish;
				}catch(Exception){}
			}
			
			private void OnMove(LevelChanges changes)
			{
				Gtk.Application.Invoke(delegate 
				{
					levelDrawer.PutChanges(changes);
				});
			}
			
			private void OnFinish(LevelSnapshot instant)
			{
				Gtk.Application.Invoke(delegate 
				{
					Console.WriteLine("Level Finished");
					super.moveIsDisabled = true;
					LosePopUp();
					super.EndGame();
				});
			}
			

			private void LosePopUp()
			{
				MessageDialog msdSame = new MessageDialog(super.MainWindow, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "Player "+opponent.Name+" wins.");
				msdSame.Title="You Lose";
				msdSame.Run();
				msdSame.Destroy();
			}
			
		}
			
			
		public bool HandleException(Exception e)
		{
			if (e is System.Runtime.Remoting.RemotingException) {
				// Level, LevelSet or GameManager could be down. Relogin for restart it
				MessageDialog msdSame = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Error, ButtonsType.YesNo, "Multiplayer service is down. You can try again to prove if it is now solved.");
   				msdSame.Title="Error";
				ResponseType reply = (ResponseType) msdSame.Run();
				msdSame.Destroy();
				
				if (reply == ResponseType.Yes){
					ConnectToMultiplayerService();
				}
			} else if (e is System.Net.WebException) {
				ErrorNotify.ModalMessageDialog(MainWindow, "Error de Red", "Parece que hay un problema de comunicación de red. Inténtelo más tarde");
			} else {
				ErrorNotify.ModalMessageDialog(MainWindow, "Error", e.Message);
			}
			return true;
		}

	}
	


}