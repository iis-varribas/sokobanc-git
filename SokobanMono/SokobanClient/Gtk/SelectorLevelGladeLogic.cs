// For fast switch between local or remote execution
// Using DEGUB for HTTP and Release for TCP
//#define REMOTE

using System;
using System.Collections.Generic;


using Gdk;
using Gtk;
using Glade;

using Sokoban.API;
using Sokoban.Library;

namespace SokobanClient.GtkUI
{
	
	public class SelectorLevelGladeLogic : IWindowGladeConnector
	{
		// Main window:
		[Widget] 
		private Gtk.Dialog dialog1;
		

		// Text area
		[Widget] 
		private Gtk.VBox vbox_Content;
		
		
		private Gtk.RadioButton rb_base = new Gtk.RadioButton("Group");
		
		private IDictionary<string, int> levels = new Dictionary<string, int>();

	
		public SelectorLevelGladeLogic() 
		{
			// Create the main window
			Glade.XML gxml = new Glade.XML("SokobanClient.Gtk.Selector.glade", "dialog1");
			gxml.Autoconnect (this);
			dialog1.Show();
			
			foreach (LevelDescription level in MyGame.Instance.CurrentLevelSet.Levels){
				Gtk.RadioButton rb = new Gtk.RadioButton(rb_base, level.Name);
				levels[level.Name] = level.LevelNr;
				vbox_Content.Add(rb);
			}
			vbox_Content.ShowAll();
		}
		
		public Gtk.Window Window
		{ 
			get {return dialog1;}
		}

		
		#region Properties

		#endregion
		
		
		#region EventHandlers
		
		private void ButtonOk_OnPressedEvent (object sender, EventArgs e)
		{
			foreach (Gtk.RadioButton rb in rb_base.Group)
			{
				if (rb.Active){
					MyGame.Instance.CurrentLevel.ChangeLevel(levels[rb.Label]);
				}
			}
			
			dialog1.Hide();
			dialog1.Dispose();
		}
		
		private void ButtonCancel_OnPressedEvent (object sender, EventArgs e)
		{
			dialog1.Hide();
			dialog1.Dispose();	
		}
		
		#endregion
	}

}