// For fast switch between local or remote execution
// Using DEGUB for HTTP and Release for TCP
//#define REMOTE

using System;


using Gdk;
using Gtk;
using Glade;

using Sokoban.ClientConnector;

namespace SokobanClient.GtkUI
{
	
	public class SelectorLevelSetGladeLogic : IWindowGladeConnector
	{
		// Main window:
		[Widget] 
		private Gtk.Dialog dialog1;
		

		// Text area
		[Widget] 
		private Gtk.VBox vbox_Content;
		
		
		private Gtk.RadioButton rb_base = new Gtk.RadioButton("Group");

	
		public SelectorLevelSetGladeLogic() 
		{
			// Create the main window
			Glade.XML gxml = new Glade.XML("SokobanClient.Gtk.Selector.glade", "dialog1");
			gxml.Autoconnect (this);
			dialog1.Show();

			//TODO: migrate to SokobanAPI.GetLevelSets()
			foreach (string levesetname in DatabaseInterfaces.Database.GetConnection().GetAllRegisteredLevelSets()){
				vbox_Content.Add(new Gtk.RadioButton(rb_base, levesetname));
			}
			vbox_Content.ShowAll();
		}
		
		public Gtk.Window Window
		{ 
			get {return dialog1;}
		}

		
		#region Properties

		#endregion
		
		
		#region EventHandlers
		
		private void ButtonOk_OnPressedEvent (object sender, EventArgs e)
		{
			foreach (Gtk.RadioButton rb in rb_base.Group)
			{
				if (rb.Active){
					MyGame.Instance.CurrentLevelSet.ChangeLevelSet(rb.Label);
				}
			}
			
			dialog1.Hide();
			dialog1.Dispose();
		}
		
		private void ButtonCancel_OnPressedEvent (object sender, EventArgs e)
		{
			dialog1.Hide();
			dialog1.Dispose();	
		}
		
		#endregion
	}

}