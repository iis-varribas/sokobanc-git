using Sokoban.Library;

namespace SokobanClient
{
	public class GtkLevelDrawer
	{
		// current frame
		protected Gdk.Pixbuf frame;
		
		private Gtk.DrawingArea drawArea;
		private GraficalLevel glevel;
		
		private float scale;
		
		public GtkLevelDrawer (Gtk.DrawingArea drawingArea, float scale)
		{
			this.scale = scale;
			this.drawArea = drawingArea;
			drawArea.ExposeEvent += OnExposeEvent;
		}
		
		
		public void PutLevel(LevelSnapshot levelInstant, int levelWidth, int levelHeight)
		{
			glevel = new GraficalLevel(levelWidth, levelHeight, scale);
			
			PutInstant(levelInstant);
		}
		
		
		public void PutInstant(LevelSnapshot instant)
		{
			glevel.drawLevel(instant);
			CreateLevelPicture();
		}
		
		public void PutChanges(LevelChanges changes)
		{
			glevel.drawChanges(changes);
			CreateLevelPicture();
		}


		private void CreateLevelPicture()
		{
			System.Drawing.Image image = glevel.Image;
			if (image != null){
				if (frame == null || image.Height != frame.Height || image.Width != frame.Width)
					frame  = new Gdk.Pixbuf (Gdk.Colorspace.Rgb, true, 8, image.Width, image.Height);

			    using (System.IO.MemoryStream stream = new System.IO.MemoryStream()) {
					image.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
					stream.Position = 0;
					Gdk.Pixbuf pixbuf = new Gdk.Pixbuf(stream);
					pixbuf.CopyArea (0, 0, frame.Width, frame.Height, frame, 0, 0);
				}
				drawArea.SetSizeRequest(frame.Width, frame.Height);
				drawArea.QueueDraw();
			}else{
				frame = null;
			}
		}
			
		
		private void PaintLevelPicture()
		{
			if (frame != null)
			{
				frame.RenderToDrawableAlpha(
					drawArea.GdkWindow,
					0, 0,
					0, 0,
					frame.Width, frame.Height,
					Gdk.PixbufAlphaMode.Full, 8,
					Gdk.RgbDither.Normal,
					100, 100
				);
			}
		}
		
		
		private void OnExposeEvent (object o, Gtk.ExposeEventArgs args)
		{		
			PaintLevelPicture();
			// return true because we've handled this event, so no
			// further processing is required.
			args.RetVal = true;
		}
	}
}

