using System;
using System.Drawing;

using Sokoban.Library;


namespace SokobanClient
{
	public class LevelImages
	{
		private LevelImages ()
		{
		}
		
		// ITEM_SIZE is the size of an item in the level
        // TO DO: Let user change it, and save the size in the savegame.xml ???
	    public static int ITEM_SIZE = 30;
		
		#region GetLevelImage
		
		/// <summary>
		/// Depending on the 'item character' in the XML for the level set we
		/// need to display an image on the screen. This is what happens here.
		/// We also take into account the direction Sokoban is moving in,
		/// because we want him to face to the left when he is moving left.
		/// </summary>
		/// <param name="itemType">Level item</param>
		/// <param name="direction">Sokoban direction</param>
		/// <returns>The image to be displayed on screen</returns>
		public static Image GetLevelImage(ItemType itemType, MoveDirection direction)
		{
		    Image image;
		    
            if (itemType == ItemType.Wall)
                image = ImgWall;
            else if (itemType == ItemType.Floor)
                image = ImgFloor;
            else if (itemType == ItemType.Package)
                image = ImgPackage;
            else if (itemType == ItemType.Goal)
                image = ImgGoal;
            else if (itemType == ItemType.Sokoban)
            {
                if (direction == MoveDirection.Up)
                    image = ImgSokoUp;
                else if (direction == MoveDirection.Down)
                    image = ImgSokoDown;
                else if (direction == MoveDirection.Right)
                    image = ImgSokoRight;
                else
                    image = ImgSokoLeft;
            }
            else if (itemType == ItemType.PackageOnGoal)
                image = ImgPackageGoal;
            else if (itemType == ItemType.SokobanOnGoal)
            {
                if (direction == MoveDirection.Up)
                    image = ImgSokoUpGoal;
                else if (direction == MoveDirection.Down)
                    image = ImgSokoDownGoal;
                else if (direction == MoveDirection.Right)
                    image = ImgSokoRightGoal;
                else
                    image = ImgSokoLeftGoal;
            }
            else
                image = ImgSpace;
            
            return image;
		}
		
        
        // These are the proprties for the images of all possible items within
        // the game. These are hard coded. If we want to ad support for skins,
        // than we should put these values inside a skin XML file.
        
        public static Image ImgWall
        {
            get { return
                Image.FromFile("graphics/wall.bmp"); }
        }
		
        public static Image ImgFloor
        {
            get { return
                Image.FromFile("graphics/floor.bmp"); }
        }
        
        public static Image ImgPackage
        {
            get { return
                Image.FromFile("graphics/package.bmp"); }
        }
        
        public static Image ImgPackageGoal
        {
            get { return
                Image.FromFile("graphics/package_goal.bmp"); }
        }
        
        public static Image ImgGoal
        {
            get { return
                Image.FromFile("graphics/goal.bmp"); }
        }
        
        public static Image ImgSokoUp
        {
            get { return
                Image.FromFile("graphics/soko_up.bmp"); }
        }
        
        public static Image ImgSokoDown
        {
            get { return
                Image.FromFile("graphics/soko_down.bmp"); }
        }
        
        public static Image ImgSokoRight
        {
            get { return
                Image.FromFile("graphics/soko_right.bmp"); }
        }
        
        public static Image ImgSokoLeft
        {
            get { return
                Image.FromFile("graphics/soko_left.bmp"); }
        }
        
        public static Image ImgSokoUpGoal
        {
            get { return
                Image.FromFile("graphics/soko_goal_up.bmp"); }
        }
        
        public static Image ImgSokoDownGoal
        {
            get { return
                Image.FromFile("graphics/soko_goal_down.bmp"); }
        }
        
        public static Image ImgSokoRightGoal
        {
            get { return
                Image.FromFile("graphics/soko_goal_right.bmp"); }
        }
        
        public static Image ImgSokoLeftGoal
        {
            get { return
                Image.FromFile("graphics/soko_goal_left.bmp"); }
        }
        
        public static Image ImgSpace
        {
            get { return
                Image.FromFile("graphics/space.bmp"); }
        }
        
        #endregion
	}
}

