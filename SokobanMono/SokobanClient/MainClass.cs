using System;


namespace SokobanClient.GtkUI
{
	public class MainClass
	{
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

			//Captura de exceptiones
			ErrorNotify.WriteUnhandledInConsole = true;

			System.Runtime.Remoting.RemotingConfiguration.Configure("ClientPorts.config", false);
			
			// Create the program
			Gtk.Application.Init();
			new MainWindowGladeLogic();
			Gtk.Application.Run();
			
        }
		
		


	}
}

