using System;
using System.Collections;
using System.Threading;


using SokobanLibrary;

using SokobanAPI.LevelEvents;

namespace SokobanClient
{
	public delegate void MessageHandler(IMessage msg);
	public interface IMessage
	{
		string Type
		{
			get;
		}
	}
	
	
	public interface IOpponent
	{		
		
		string Name
		{
			get;
		}
		
		event MessageHandler MessageNotifier;
		
		event OnChangeHandler OnMove;
		event OnFinishHandler OnFinish;
		
	}
	
	
	
	public class Player : MarshalByRefObject, IOpponent
	{		
		private string name;
		
		public Player (string name)
		{
			this.name = name;
		}
		
		public string Name
		{
			get {return name;}
		}
			
		public void SendMessage(IMessage msg)
		{
		}
		
		public void SendMove (LevelChanges change)
		{
			if (OnMove == null) return;
			
			OnMove(change);
		}
		
		public void SendFinish (LevelDescription change)
		{
			if (OnFinish == null) return;
			
			OnFinish(change);
		}
		
		public event MessageHandler MessageNotifier;
		public event OnChangeHandler OnMove;
		public event OnFinishHandler OnFinish;

	}
	
	public class Multiplayer : MarshalByRefObject
	{
		private Player itself;
		private ArrayList opponents;
		private LevelAbstract thelevel;
		
		
		public Multiplayer(Player itself, IList opponents, LevelAbstract thelevel)
		{
			this.itself = itself;
			this.opponents = new ArrayList(opponents);
			this.thelevel = thelevel;
		}
		
		public Player MySelf
		{
			get { return itself; }
		}
		
		public LevelAbstract Level
		{
			get {return thelevel;}
		}
		
		public IOpponent this[int index]
		{
			get { return (IOpponent) opponents[0]; }
		}
		
		public int NrOfOpponents
		{
			get { return opponents.Count; }
		}
	}
	
	
	public class MultiplayerGame : MarshalByRefObject
	{
		private static ArrayList players = new ArrayList();
		
		public static Multiplayer Join(Player p)
		{
			Multiplayer mp;
			Monitor.Enter(players);
			
			LevelSetAbstract levelSet = null;//new GameServer.LevelSet();
			levelSet.SetLevelSet("boxworld.xml");
			levelSet.SetLevelsInLevelSet("boxworld.xml");
			Console.WriteLine(levelSet.NrOfLevels);
			LevelAbstract level = levelSet[0];
			ArrayList opp = new ArrayList();
			opp.Add(p);opp.Add(p);
			mp = new Multiplayer(p, opp, level);
//			Monitor.Wait(players, TimeSpan.FromSeconds(3));
			
//			players.Add(p);
//			if (players.Count == 2 ){
//				ArrayList opp = new ArrayList(players);
//				opp.Remove(p);
//				mp = new Multiplayer(p, opp);
//			}else{
//				Monitor.Wait(players, TimeSpan.FromSeconds(30));
//				throw new Exception("No players to play");
//			}
				             
			
			Monitor.Exit(players);
			return mp;	
		}
		
	}
}

