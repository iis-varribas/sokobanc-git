// For fast switch between local or remote execution
// Using DEGUB for HTTP and Release for TCP

//#define REMOTE

using System;
#if REMOTE
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;

using System.Collections;
using System.Collections.Generic;

using Sokoban.ClientConnector;
using RemotingLifeTimeTools;
#else
using Sokoban.ClientConnector;
#endif

using Sokoban.API;

namespace SokobanClient
{
	public abstract class MyGame
	{
		private static IGame instance;
#if REMOTE
		private static BasicSponsor sponsor;
#endif
		
		public static  IGame Instance 
		{
			get 
			{
				if (instance == null) instance = CreateGame();
				return instance; 
			}
		}
		
		public static event EventHandler OnExit;
		public static void Exit(){
			if (OnExit != null) try{OnExit(null, null);}catch(Exception){}
			
			if (instance != null) instance.Terminate();
#if REMOTE
				Console.WriteLine("Removing Sponsor from "+instance);
				sponsor.Enable = false;
#endif
			Gtk.Application.Quit();//TODO: gtk dependency?
		}
		
		
		private static IGame CreateGame()
		{
#if REMOTE
            IGameAbstractFactory factory = (IGameAbstractFactory) Activator.GetObject(
				typeof(IGameAbstractFactory),
#if DEBUG
				"http://localhost:2002/IGameFactory"
#else                                                   
                "tcp://localhost:2002/IGameFactory"	
#endif
			);

			sponsor = new BasicSponsor();
			IGame game = factory.CreateGame(sponsor);
            return game;
#else
			return new GameAPI(0);
#endif
		}

	}
}

