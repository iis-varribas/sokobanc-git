using System;
using System.Runtime.Remoting.Lifetime;
using RemotingLifeTimeTools;

namespace Sokoban.ClientConnector
{
	public partial class GameAPI  : MarshalByRefObjectWithLease, Sokoban.API.IGame
	{
		private GameImpl gamelogic;	
		private int ClientUID;
		
		
		public GameAPI(int ClientUID)
		{
			Console.WriteLine("GameAPI.__NEW__() ["+ClientUID+"]");
			this.ClientUID = ClientUID;
			
			gamelogic = new GameImpl();
			_LevelSet = new CurrentLevelSetClass(gamelogic);
			_Level = new CurrentLevelClass(gamelogic);
			_User = new CurrentUserClass(gamelogic);
			_Move = new MovementClass(gamelogic);
			
		}
		
        ~GameAPI()
        {
            Console.WriteLine("GameAPI.__DESTROY__() ["+ClientUID+"]");

        }
		
		
		public void Terminate()
		{
			Console.WriteLine("GameAPI.Terminate() ["+ClientUID+"]");
			gamelogic.Terminate();
			
			ILease lease = (ILease)base.GetLifetimeService();
			if (lease != null) Console.WriteLine("Lease: State [{0}] Time [{1}]", lease.CurrentState, lease.CurrentLeaseTime);
			
			Console.WriteLine("Disconnecting... "+System.Runtime.Remoting.RemotingServices.Disconnect(this));
		}
		
		

		public override object InitializeLifetimeService()
        {
			
			ILease lease = (ILease) base.InitializeLifetimeService();

			lease.InitialLeaseTime = TimeSpan.FromSeconds(10);
			lease.SponsorshipTimeout = TimeSpan.FromSeconds(10);
			lease.RenewOnCallTime = TimeSpan.FromSeconds(10);

			
			Console.WriteLine("Lease: State [{0}] Time [{1}]", lease.CurrentState, lease.CurrentLeaseTime);
			
    		return lease;
        }

	}

}

