using Sokoban.Library;
using Sokoban.API;
using Sokoban.API.LevelEvents;

namespace Sokoban.ClientConnector
{
	public partial class GameAPI
	{
		private CurrentLevelClass _Level;
		
		public ICurrentLevel CurrentLevel
		{
			get {return _Level;}
		}
	}
	
		
	class CurrentLevelClass : System.MarshalByRefObject, Sokoban.API.ICurrentLevel
	{
		private GameImpl logic;
		internal CurrentLevelClass(GameImpl logic)
		{
			this.logic = logic;
		}
			
        public string Name
        {
            get {return logic.cachedLevelInfo.Name;}
        }
		
		public string LevelSetName
        {
            get {return logic.cachedLevelInfo.LevelSetName;}
        }

        public int LevelNr
        {
            get {return logic.cachedLevelInfo.LevelNr;} 
        }

        public int Width
        {
            get {return logic.cachedLevelInfo.Width;}
        }

        public int Height
        {
            get {return logic.cachedLevelInfo.Height;}
        }
		
        public LevelDescription LevelDescription
        {
            get {return logic.cachedLevelInfo;}
        }
		
			

		
		public void NextLevel()
		{
			logic.Level_NextLevel();
		}
		
		public void ChangeLevel(int level_number)
		{
			logic.Level_Change(level_number);
		}
		
		public LevelChanges GetChanges()
		{
			return logic.Level_GetChanges();	
		}
		
		public LevelSnapshot GetInstant()
		{
			return logic.Level_GetInstant();	
		}
		
		public event OnLoadHandler OnLoad
		{
			add { logic.Level_OnLoad += value; }
			remove { logic.Level_OnLoad -= value; }
		}
					
		public event OnChangeHandler OnChange
		{
			add { logic.Level_OnChange += value; }
			remove { logic.Level_OnChange -= value; }
		}
		public event OnFinishHandler OnFinish
		{
			add { logic.Level_OnFinish += value; }
			remove { logic.Level_OnFinish -= value; }
		}
	}

}

