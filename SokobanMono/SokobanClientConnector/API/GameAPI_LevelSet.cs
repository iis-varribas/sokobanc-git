using System;

using System.Collections.Generic;
using Sokoban.Library;
using Sokoban.API;
using Sokoban.API.LevelSetEvents;

namespace Sokoban.ClientConnector
{
	public partial class GameAPI
	{
		private CurrentLevelSetClass _LevelSet;
		
		public ICurrentLevelSet CurrentLevelSet
		{
			get {return _LevelSet;}
		}

	}
	
	
	class CurrentLevelSetClass : MarshalByRefObject,  ICurrentLevelSet
	{
		private GameImpl logic;
		internal CurrentLevelSetClass(GameImpl logic)
		{
			this.logic = logic;
		}
		
		public void ChangeLevelSet(string levelset_name)
		{
			logic.LevelSet_Load(levelset_name);
		}
		
		public event OnLoadHandler OnLoad
		{
			add { logic.LevelSet_OnLoad += value; }
			remove { logic.LevelSet_OnLoad -= value; }
		}
		

		public LevelSetDescription LevelSetDescription
        {
            get {return logic.cachedLevelSetInfo;}
        }
		
		
		public LevelDescription[] Levels
		{
			get { return logic.LevelSet_GetDataLevels();}
		}
		
		
		
        public string Title
        {
            get {return logic.cachedLevelSetInfo.Title;}
        }

        public string Description
        {
            get {return logic.cachedLevelSetInfo.Description;}
        }
 
        public string Email
        {
            get {return logic.cachedLevelSetInfo.Email;}
        }
 
        public string Url
        {
            get {return logic.cachedLevelSetInfo.Url;}
        }
 
        public string Author
        {
            get {return logic.cachedLevelSetInfo.Author;}
        }
		
		public int NrOfLevels
        {
            get {return logic.cachedLevelSetInfo.NrOfLevelsInSet;}
        }
	}

}

