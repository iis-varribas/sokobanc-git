using Sokoban.API;
	
namespace Sokoban.ClientConnector
{
	public partial class GameAPI
	{
		private MovementClass _Move;
		
		public IMovementExtra Move
		{
			get {return _Move;}
		}
	}
	
		
	class MovementClass : System.MarshalByRefObject, IMovementExtra
	{
		private GameImpl logic;
		internal MovementClass(GameImpl logic)
		{
			this.logic = logic;
		}
			
		public void Up()
		{
			logic.Move_Up();

		}
		
		public void Down()
		{
			logic.Move_Down();

		}
		
		public void Left()
		{
			logic.Move_Left();

		}
		
		public void Right()
		{
			logic.Move_Right();

		}
		
		public void Reset()
		{
			logic.Level_Reset();	
		}
	}

}

