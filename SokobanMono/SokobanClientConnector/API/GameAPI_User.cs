using Sokoban.API;
	
namespace Sokoban.ClientConnector
{
	public partial class GameAPI
	{
		private CurrentUserClass _User;
		
		public ICurrentUser User
		{
			get {return _User;}
		}
	}
	
		
	class CurrentUserClass : System.MarshalByRefObject, ICurrentUser
	{
		private GameImpl logic;
		internal CurrentUserClass(GameImpl logic)
		{
			this.logic = logic;
		}
			
		public string Name
		{ 
			get { return logic.user.name; }
		}
		
		public string User
		{ 
			get { return logic.user.user; }
		}
		
		
		public string LastLevelSetPlayed
		{
			get { return ""; }
		}
			

		
		public bool Login(string user, string passwd)
		{
			System.Console.WriteLine("GameAPI.Login({0}, {1})", user, passwd);
			return logic.User_Login(user, passwd);
			
		}
		
		public bool Register(string user, string passwd)
		{
			System.Console.WriteLine("GameAPI.Register({0}, {1})", user, passwd);
			return logic.User_Register(user, passwd);
		}
	}

}

