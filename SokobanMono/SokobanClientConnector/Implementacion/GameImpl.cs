using System;

using Sokoban.Library;
namespace Sokoban.ClientConnector
{
	internal partial class GameImpl
	{
		
		public GameImpl()
		{
			User_Initialize();
			UserBDSync_Initialize();
			LevelSet_Initialize();
			Level_Initialize();
		}
		
		
		public void Terminate()
		{
			Level_Terminate();
			LevelSet_Terminate();
			UserBDSync_Terminate();
			User_Terminate();
		}
		
		
		private void CheckAccess(object o, String context)
		{
			if (o == null)	
				//throw new IllegalAccessException("Level");
				throw new Exception("IllegalAccess to uninitialized Context: "+context);
		}
	
	}
}

