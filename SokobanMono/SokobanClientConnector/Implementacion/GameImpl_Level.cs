using System;
using System.Diagnostics;
using System.Runtime.Remoting;

using Sokoban.Library;
using Sokoban.API.LevelEvents;

namespace Sokoban.ClientConnector
{
	internal partial class GameImpl
	{
		#region Commons
		
		protected LevelAbstract level;
		internal LevelDescription cachedLevelInfo;
		
		#endregion
		
		

		#region Level
		
		private void Level_Initialize()
		{
			LevelSet_OnLoad += 	delegate(LevelSetDescription ls)
			{
				int level;
				if (cachedPlayedLevelSet == null){
					level = 0;
					Console.WriteLine("Is your first time in LevelSet '{0}'", cachedLevelSetInfo.Title);
				}else{
					level = cachedPlayedLevelSet.LastCompletedLevel;
					Console.WriteLine("You already played LevelSet '{0}', last level finished was '{1}'", cachedLevelSetInfo.Title, level);
					level++;
					if (level < cachedLevelSetInfo.NrOfLevelsInSet){
						Console.WriteLine("Loading Level '{0}'", level);
					}else{
						level = 0;
						Console.WriteLine("You already finished the LevelSet, loading first Level");
					}
				}
				Level_Change(level);
			};
		}
		
		private void Level_Terminate()
		{
			//TODO: Close level and send progress.
			if (elapsedPlayedLevelTime != null) {
				elapsedPlayedLevelTime.Stop();
				elapsedPlayedLevelTime = null;
			}
			if (level != null) {
				Level_Drop(level);
				level = null;
			}
		}
		
		public void Level_NextLevel()
		{
			if (level != null){
				if (level.IsFinished()){
					int next_level_id = cachedLevelInfo.LevelNr+1;
					if (next_level_id == cachedLevelSetInfo.NrOfLevelsInSet){
						// Send SevelSet.OnFinish
						Console.Write("LevelSet finished");
						throw new Exception("LevelSet finished");
					}else{
						Console.Write("NextLevel ->");
						Level_Change(next_level_id);
					}
				}else{
					throw new Exception("Next level can't be loaded if current has not benn completed");
				}
			}
		}

		
		public void Level_Change(int level_number)
		{
			// Obtain the new level
			LevelAbstract new_level;
			try{
				new_level = levelSet[level_number];
				new_level.RegisterSponsor(LevelSet_sponsor);
			}catch(Exception){
				throw new Exception("Level '"+level_number+"' from LevelSet '"+levelSet+"' could not be recovered");
			}
			
			// Forget/free previous level if exists
			if (level != null) Level_Drop(level);
				
			level = new_level;
			Level_CallEvent_OnLoad();
		}
		
		public void Level_Reset()
		{
			Level_CheckAccess(level);
			
			level.ResetLevel();
			Level_CallEvent_OnChange();
		}
		
		private void Level_Drop(LevelAbstract level)
		{
			try{
				level.DeregisterSponsor(LevelSet_sponsor);
			}catch(RemotingException){
				Console.WriteLine("[Warning] Unable to Deregister Level sponsor");
			}
		}
		
		
		public void Move_Up()
		{
			Level_CheckAccess(level);
			
			if (level != null && level.MoveSokoban(MoveDirection.Up))
				Level_CallEvent_OnChange();
		}
		
		public void Move_Down()
		{
			Level_CheckAccess(level);
			
			if (level != null && level.MoveSokoban(MoveDirection.Down))
				Level_CallEvent_OnChange();
		}
		
		public void Move_Left()
		{
			Level_CheckAccess(level);
			
			if (level != null && level.MoveSokoban(MoveDirection.Left))
				Level_CallEvent_OnChange();
		}
		
		public void Move_Right()
		{
			Level_CheckAccess(level);
			
			if (level != null && level.MoveSokoban(MoveDirection.Right))
				Level_CallEvent_OnChange();
		}
			
		
		public void Move_Undo()
		{
			Level_CheckAccess(level);
			
			level.Undo();
			Level_CallEvent_OnChange();
		}
		
		public void Move_Redo()
		{
			Level_CheckAccess(level);
			
			level.Redo();
			Level_CallEvent_OnChange();
		}
		
		
		
		public LevelChanges Level_GetChanges()
		{
			Level_CheckAccess(level);
			
			return level.GetChanges();
		}
		
		public LevelSnapshot Level_GetInstant()
		{
			Level_CheckAccess(level);
			
			return level.GetInstant();
		}
		
		public event OnLoadHandler Level_OnLoad;
		public event OnChangeHandler Level_OnChange;	
		public event OnFinishHandler Level_OnFinish;
		
		


		
		
		private void Level_CallEvent_OnLoad()
		{
			Level_CheckAccess(cachedLevelInfo);
			
			cachedLevelInfo = level.GetDescription();
			if (Level_OnLoad != null) Level_OnLoad(cachedLevelInfo);
		}
		
		private void Level_CallEvent_OnChange()
		{
			Level_CheckAccess(level);
			
			LevelChanges changes = level.GetChanges();
			
			if (Level_OnChange != null) Level_OnChange(changes);

			if (level.IsFinished())
				Level_CallEvent_OnFinish();
		}
		
		private void Level_CallEvent_OnFinish()
		{
			if (Level_OnFinish != null) Level_OnFinish(level.GetDescription());
		}
		
		
		private void Level_CheckAccess(object o)
		{
			if (o == null)	
				//throw new IllegalAccessException("Level");
				throw new Exception("IllegalAccess to uninitialized Context: Level");
		}
		
		#endregion

	
	}
}

