// For fast switch between local or remote execution
// Using DEGUB for HTTP and Release for TCP

#define REMOTE


#if REMOTE
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;

using System.Collections;
using System.Collections.Generic;


using RemotingLifeTimeTools;

using Sokoban.GameServer.Interfaces;
using UserDefined.Naming;
#else
using Sokoban.GameServer;
#endif

using System;
using Sokoban.Library;
using Sokoban.API.LevelSetEvents;


namespace Sokoban.ClientConnector
{
	internal partial class GameImpl
	{
		#region Commons
		
		protected LevelSetAbstract levelSet;
		internal LevelSetDescription cachedLevelSetInfo;
		internal String lastLevelSetPlayed;
		private LevelDescription[] cachedLevelsInfos;
		private IGameManager manager;
		
		#endregion
		
#if REMOTE
		private BasicSponsor LevelSet_sponsor;
#endif	
		
		#region LevelSet
		
		private void LevelSet_Initialize()
		{
			User_OnLogin += delegate(object sender, EventArgs e) { CreateLevelSet(); };
			
#if REMOTE
			LevelSet_sponsor = new BasicSponsor();
			
			//Register a channel for Remoting communication
			IDictionary propBag = new Hashtable();
			propBag["port"] = 0;
			propBag["typeFilterLevel"] = TypeFilterLevel.Full;
			propBag["name"] = "Channel_GameImpl_LevelSet";
		
#if DEBUG
			SoapServerFormatterSinkProvider provider = new SoapServerFormatterSinkProvider();
			provider.TypeFilterLevel = TypeFilterLevel.Full;
			HttpChannel channel = new HttpChannel(propBag, null, provider);
#else
			BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
			provider.TypeFilterLevel = TypeFilterLevel.Full;
			TcpChannel channel = new TcpChannel(propBag, null, provider);
#endif
			Console.WriteLine("*Channel Name: "+channel.ChannelName);
            ChannelServices.RegisterChannel(channel, false);
#endif
		}
		
		private void LevelSet_Terminate()
		{
			//TODO: Close levelset, connections and send progress.
			if (levelSet != null) {
				LevelSet_Drop(levelSet);
				levelSet = null;
			}
#if REMOTE
			LevelSet_sponsor.Enable = false;
#endif
		}
		
		
		
		public void LevelSet_Load(string name)
		{
			// Obtain the new levelSet
			LevelSetAbstract new_levelset = manager.GetLevelSet(name);
			if (new_levelset == null){
				throw new Exception("LevelSet '"+name+"' could not be recovered");
			}
			new_levelset.RegisterSponsor(LevelSet_sponsor);
			
			// Forget/free previous levelset if exists
			if (levelSet != null) LevelSet_Drop(levelSet);
				
			levelSet = new_levelset;
			LevelSet_CallEvent_OnLoad();
		}
		
		
		public event OnLoadHandler LevelSet_OnLoad;
		
		
			

		public LevelDescription[] LevelSet_GetDataLevels()
		{
			if (cachedLevelsInfos == null)
			{
				cachedLevelsInfos = levelSet.LevelsInfos;
			}
			return cachedLevelsInfos;
			
		}
		

		private void LevelSet_Drop(LevelSetAbstract levelSet)
		{
			try{
				levelSet.DeregisterSponsor(LevelSet_sponsor);
			}catch(RemotingException){
				Console.WriteLine("[Warning] Unable to Deregister LevelSet sponsor");
			}
		}

		
		/// <summary>
	    /// Sets the data for PlayerData, LevelSet, etc..
	    /// </summary>
	    private void CreateLevelSet()
	    {
#if REMOTE
			string remoteObjectURI;
			try {
				remoteObjectURI = ExternalNamingService.LookUp("GameManagerFactory.rem");
			}catch(Exception){
				Console.WriteLine("[ERROR] LookUp fail. Trying with hardcored uri");
#if DEBUG
                remoteObjectURI = "http://localhost:2000/GameManagerFactory.rem";
#else
				remoteObjectURI = "tcp://localhost:2000/GameManagerFactory.rem";
#endif
			}
			
            IGameManagerFactory factory;
			try{
				factory = (IGameManagerFactory) Activator.GetObject(
					typeof(IGameManagerFactory),
					remoteObjectURI
				);
				
	            manager = factory.CreateManager();
				manager.RegisterSponsor(LevelSet_sponsor);
				
			}catch(Exception e){
				throw new Exception("Unable to connect to LevelSet Server", e);
			}	
#else
			levelSet = new LevelSet();
#endif
		}
		
		
		
		private void LevelSet_CallEvent_OnLoad()
		{
			cachedLevelsInfos = null;
			levelSet.CurrentLevel = 0; //FIXME
			cachedLevelSetInfo = levelSet.GetDescription();
			lastLevelSetPlayed = cachedLevelSetInfo.Title;
			
			if (LevelSet_OnLoad != null) LevelSet_OnLoad(cachedLevelSetInfo);

		}
		#endregion

		
		
	}
}

