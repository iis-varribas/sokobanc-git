using System;
using System.Collections.Generic;

using Sokoban.Library;

namespace Sokoban.ClientConnector
{
	internal partial class GameImpl
	{
		#region Commons
		
		internal User user;
		
		#endregion
		

		#region User
		
		private void User_Initialize()
		{
			
		}
		
		private void User_Terminate()
		{
			//TODO: Close user and logout.
			user = null;
		}
		
		[Serializable]
		internal class User
		{
			internal static Dictionary<string, string> users = new Dictionary<string, string>();
			
			public readonly int id;
			public readonly string name;
			public readonly string user;
			[NonSerialized] public readonly string pass;
			
			internal User(string name, int id, string user, string pass)
			{
				this.id = id;
				this.name = name;
				this.user = user;
				this.pass = pass;
			}
			
		}
		
		
		public bool User_Login(string user_name, string user_passwd)
		{
			if (DatabaseInterfaces.Database.GetConnection().UserLogin(
			                                                        user_name, 
			                                                        DatabaseInterfaces.Encoder.EncodePassword(user_passwd)
			)){
				user = new User(user_name, DatabaseInterfaces.Database.GetConnection().GetUserID(user_name), user_name, user_passwd);
				Console.WriteLine("User ID: "+user.id);
				User_CallEvent_OnLogin();
				return true;
			}else{
				return false;
			}
			
		}
		
		public bool User_Register(string user, string passwd)
		{
//			if (!User.users.ContainsKey(user)){
//				User.users[user] = passwd;
			passwd = DatabaseInterfaces.Encoder.EncodePassword(passwd);
			if (DatabaseInterfaces.Database.GetConnection().UserRegister(user, user, null, passwd)){
				return true;
			}else{
				return false;
			}
		}
		
		
		public event EventHandler User_OnLogin;
		
		
		private void User_CallEvent_OnLogin()
		{
			
			if (User_OnLogin != null) User_OnLogin(user, null);
			
			//FIXME
//			foreach (DatabaseInterfaces.PlayedLevelSet played in DatabaseInterfaces.Database.GetConnection().GetPlayedSetsForUserID(user.id)){
//				Console.WriteLine("{0} {1} {2}", played.Id, played.Name, played.LastCompletedLevel);	
//			}
			//TODO DatabaseInterfaces.Instance.GetDatabase().GetAllRegisteredLevelSets();
			
		}
		
		#endregion
	}
}

