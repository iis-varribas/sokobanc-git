using System;
using System.Linq;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Collections.Generic;

using Sokoban.Library;
using Sokoban.API.LevelEvents;
using DatabaseInterfaces;

namespace Sokoban.ClientConnector
{
	internal partial class GameImpl
	{
		#region Commons

		private Stopwatch elapsedPlayedLevelTime;
		private long elapsedPlayedLevelSetTime;
		private int currentLevelSetID;
		private List<PlayedLevelSet> playedLevelSetsList;
		internal PlayedLevelSet cachedPlayedLevelSet;
		
		#endregion
		
		

		#region Level
		
		private void UserBDSync_Initialize()
		{
			User_OnLogin += UserBDSync_User_OnLogin;
			LevelSet_OnLoad += UserBDSync_LevelSet_OnLoad;
			Level_OnLoad += UserBDSync_Level_OnLoad;
			Level_OnFinish +=  UserBDSync_Level_OnFinish;
			
			elapsedPlayedLevelTime = new Stopwatch();
		}
		
		private void UserBDSync_Terminate()
		{
			Level_OnLoad -= UserBDSync_Level_OnLoad;
			Level_OnFinish -=  UserBDSync_Level_OnFinish;
			LevelSet_OnLoad -= UserBDSync_LevelSet_OnLoad;
			User_OnLogin -= UserBDSync_User_OnLogin;
			
			if (elapsedPlayedLevelTime != null){
				elapsedPlayedLevelTime.Stop();
				elapsedPlayedLevelTime = null;
			}
			//TODO: Close and send progress.
		}
		


		private void UserBDSync_User_OnLogin(object sender, EventArgs e)
		{
			playedLevelSetsList = DatabaseInterfaces.Database.GetConnection().GetPlayedSetsForUserID(user.id);
		}
		
		
		private void UserBDSync_LevelSet_OnLoad(LevelSetDescription desc)
		{
			currentLevelSetID = DatabaseInterfaces.Database.GetConnection().GetLevelSetID(desc.Title);
			Console.WriteLine("Returned ID for '{0}' is '{1}'", desc.Title, currentLevelSetID);
			
			if (playedLevelSetsList == null) {
				elapsedPlayedLevelSetTime = 0;
			}else{
				// Use of LINQ and Extensions (System.Data.Linq System.Core) to obtain the levelsetinfo from a list
				try{
					PlayedLevelSet result = playedLevelSetsList.Single(obj => obj.Id == currentLevelSetID);
					elapsedPlayedLevelSetTime = result.MillisSpent;
					cachedPlayedLevelSet = result;
				}catch(InvalidOperationException){
					elapsedPlayedLevelSetTime = 0;
				}
			}
		}
		
		private void UserBDSync_Level_OnLoad(LevelDescription level)
		{
			elapsedPlayedLevelTime.Reset();
			elapsedPlayedLevelTime.Start();
		}
		

		
		private void UserBDSync_Level_OnFinish(LevelDescription level)
		{
			// Get played time
			elapsedPlayedLevelTime.Stop();
			long levelmilis = elapsedPlayedLevelTime.ElapsedMilliseconds;
			Console.WriteLine("Send change to BD");
			// Update database
			elapsedPlayedLevelSetTime += levelmilis;
			int levelSetID = DatabaseInterfaces.Database.GetConnection().GetLevelSetID(cachedLevelSetInfo.Title);
			DatabaseInterfaces.Database.GetConnection().UpdateRelationship(user.id, 
				                                                           levelSetID, 
				                                                           cachedLevelInfo.LevelNr,
				                                                           this.level.Moves, (int)elapsedPlayedLevelSetTime);
			Console.WriteLine("Sent");
		}

		#endregion

	
	}
}

