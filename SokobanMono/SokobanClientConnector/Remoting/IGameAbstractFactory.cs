﻿using System;
using System.Runtime.Remoting.Lifetime;

using Sokoban.API;

namespace Sokoban.ClientConnector
{
    public interface IGameAbstractFactory
    {
        IGame CreateGame(ISponsor client);
    }
}
