using System;
using System.Runtime.Remoting.Lifetime;

using Sokoban.API;

namespace Sokoban.ClientConnector
{
    public class IGameConctreteFactory : MarshalByRefObject, IGameAbstractFactory
    {
		private static int ClientUID;
        public IGameConctreteFactory()
        {
			Console.WriteLine("IGameConctreteFactory.__NEW__()");
		}
		
        ~IGameConctreteFactory()
        {
			Console.WriteLine("IGameConctreteFactory.__DESTROY__()");
		}

        public IGame CreateGame(ISponsor client)
        {
			Console.WriteLine("New IGame dispatched");
			GameAPI game = new GameAPI(ClientUID++);
			if (client != null){
				game.RegisterSponsor(client);
			}else{
				Console.WriteLine("Sponsor is null, I'll die soon...");
			}
			
            return game;
        }

		// IGameConctreteFactory is published as Singleton (or singlecall). Both modes
		// have his own wrapper/bottom Lease/Disconnecto management. Therefore we do 
		// not need to override InitializeLifetimeService() method.
		//
        //public override object InitializeLifetimeService()
        //{
		//	return base.InitializeLifetimeService();
        //}
	}
}

