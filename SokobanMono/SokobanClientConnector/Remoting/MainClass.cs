using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;

using System.Collections;
using System.Collections.Generic;

namespace Sokoban.ClientConnector
{
	public class MainClass
	{
        public static void Main(string[] args)
        {
            // configure a channel
			// Using DEGUB for HTTP and Release for TCP
			IDictionary propBag = new Hashtable();
			propBag["port"] = 2002;
			propBag["typeFilterLevel"] = TypeFilterLevel.Full;
			propBag["name"] = "Channel_IGameFactory";
		
#if DEBUG
			SoapServerFormatterSinkProvider provider = new SoapServerFormatterSinkProvider();
			provider.TypeFilterLevel = TypeFilterLevel.Full;
			HttpChannel channel = new HttpChannel(propBag, null, provider);
#else
			BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
			provider.TypeFilterLevel = TypeFilterLevel.Full;
			TcpChannel channel = new TcpChannel(propBag, null, provider);
#endif
			Console.WriteLine("Channel Name: "+channel.ChannelName);
            ChannelServices.RegisterChannel(channel, false);

            // publish the factory
            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(IGameConctreteFactory),
                "IGameFactory",
                WellKnownObjectMode.Singleton);

            Console.WriteLine("Server waiting for requests...");
#if DEBUG
			Console.WriteLine("[HTTP]");
#else
			Console.WriteLine("[TCP]");
#endif
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
	}
}

