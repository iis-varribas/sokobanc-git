namespace Sokoban.Library
{
	public interface ILevelProperties
	{
        #region Properties

        string Name
        {
            get;
        }
		
		string LevelSetName
        {
            get;
        }

        int LevelNr
        {
            get; 
        }

        int Width
        {
            get;
        }

        int Height
        {
            get;
        }
		
		#endregion
		
	}
}

