namespace Sokoban.Library
{
	public interface ILevelSetProperties
	{
		
		#region Properties

        string Title
        {
            get;
        }

        string Description
        {
            get;
        }

        string Email
        {
            get;
        }

        string Url
        {
            get;
        }

        string Author
        {
            get;
        }
		
		int NrOfLevels
        {
            get;
        }
		
		#endregion
	}
}

