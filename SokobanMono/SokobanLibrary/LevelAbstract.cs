﻿using System;

using RemotingLifeTimeTools;

namespace Sokoban.Library
{
    public abstract class LevelAbstract : MarshalByRefObjectWithLease, ISponsable, ILevelProperties, ICloneable
    {
	
        public LevelAbstract(
             string aName, 
             ItemType[,] aLevelMap, 
             int aWidth, int aHeight, 
             int sPosX, int sPosY, 
             int aNrOfGoals, int aLevelNr, 
             string aLevelSetName)
        {
			levelSetName = aLevelSetName;
			levelNr = aLevelNr;
            name = aName;
            width = aWidth;
            height = aHeight;
            nrOfGoals = aNrOfGoals;
            
			_levelMap = aLevelMap;
			_sokoPosX = sPosX;
			_sokoPosY = sPosY;
			this.ResetLevel();
        }
		
		
		//Level info
		protected string levelSetName;    // The name of the level set that the level belongs to
        protected string name;            // Name of the level
        protected int nrOfGoals;          // A box must be placed on a goal
        protected int levelNr;
        protected int width;              // Level width in items
        protected int height;             // Level height in items
		
		
		// Init state
		protected readonly ItemType[,] _levelMap;       // Level layout of items
		protected readonly int _sokoPosX;               // X position of Sokoban
        protected readonly int _sokoPosY;               // Y position of Sokoban


		//Game state
        protected ItemType[,] levelMap;       // Level layout of items
        protected int moves = 0;              // Sokoban number of moves
        protected int pushes = 0;             // Pushes (when a box is moved)
        protected int sokoPosX;               // X position of Sokoban
        protected int sokoPosY;               // Y position of Sokoban
        protected MoveDirection sokoDirection = MoveDirection.Right; // Default direction Sokoban is facing when starting a level.

		
        // changedItems is updated every time Sokoban moves or pushes a box.
        // Max. 3 items can be changed each push, 2 for each move. We keep
        // track of these change so we don't have to redraw the whole level
        // after each move/push.
        protected Item item1, item2, item3;


		protected bool level_reseted = true;


        #region Properties

        public string Name
        {
            get { return name; }
        }
		
		public string LevelSetName
        {
            get { return levelSetName; }
        }

        public int LevelNr
        {
            get { return levelNr; }
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }
		
		public int NrOfGoals
		{
			get { return nrOfGoals; }
		}

		#endregion
		
		
		#region GameProperties
		
		public ItemType[,] Map
		{
			get { return levelMap; }
		}
		
		public MoveDirection SokoDirection
        {
            get { return sokoDirection; }
        }

        public int Moves
        {
            get { return moves; }
        }

        public int Pushes
        {
            get { return pushes; }
        }
		
		#endregion
   
		
		#region logic
        public abstract bool IsFinished();
        public abstract bool MoveSokoban(MoveDirection direction);
		public abstract void Undo();
		public abstract void Redo();
		//public abstract LevelChanges GetChanges();
		//public abstract LevelDescription GetDescription();
		#endregion
		
		public abstract object Clone();
		
		#region Implemented Methods
		
		public void ResetLevel()
		{
			sokoPosX = _sokoPosX;
			sokoPosY = _sokoPosY;
			levelMap = (ItemType[,])_levelMap.Clone();
			sokoDirection = MoveDirection.Right;
			moves  = 0;
			pushes = 0;
			
			level_reseted = true;
		}
		
		public LevelSnapshot GetInstant()
		{
			Item[] levelcells = new Item[width*height];
			int pos = 0;
			for (int x=0; x<width; x++)
				for (int y=0; y<height; y++)
					levelcells[pos++] = new Item(levelMap[x,y], x, y);
			return new LevelSnapshot(moves,pushes, levelcells, sokoDirection, sokoPosX, sokoPosY);
		}
		
		public LevelChanges GetChanges()
		{
			Item[] changes;
			if (level_reseted)
			{
				level_reseted = false;
				changes = new Item[width*height];
				int pos = 0;
				for (int x=0; x<width; x++)
					for (int y=0; y<height; y++)
						changes[pos++] = new Item(levelMap[x,y], x, y);
			}else{
				changes = (item3 != null)? new Item[]{item1,item2,item3} : new Item[]{item1,item2};
			}
			return new LevelChanges(moves, pushes, changes, sokoDirection);
		}
		
		
		public LevelDescription GetDescription()
		{
			return new LevelDescription(levelSetName, name, levelNr, width, height, nrOfGoals);
		}
		
		#endregion
    }
}
