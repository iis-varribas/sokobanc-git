using System;
namespace Sokoban.Library
{
	[Serializable]
	public struct LevelChanges
	{
		public readonly int moves, pushes;
		public readonly Item[] changes;
		public readonly MoveDirection direction;
		
		public LevelChanges(int moves, int pushes, Item[] levelchanges, MoveDirection dir)
		{
			this.moves  = moves; 
			this.pushes = pushes;
			this.direction = dir;
			this.changes = levelchanges;
		}
		
		public static readonly LevelChanges Empty = new LevelChanges();
	}
	
}

