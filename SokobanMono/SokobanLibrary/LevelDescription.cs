using System;
namespace Sokoban.Library
{
	[Serializable]
	public struct LevelDescription
	{
		//Level info
		public readonly string LevelSetName;    // The name of the level set that the level belongs to
        public readonly string Name;            // Name of the level
		public readonly int LevelNr;
		
        public readonly int NrOfGoals;          // A box must be placed on a goal
        public readonly int Width;              // Level width in items
        public readonly int Height;             // Level height in items
		
		public LevelDescription(string levelSetName, string name, int levelNr, int width, int height, int nrOfGoals)
		{
			this.LevelSetName = levelSetName;
			this.Name = name;
			this.LevelNr = levelNr;
			this.Width = width;
			this.Height = height;
			this.NrOfGoals = nrOfGoals;
		}
	}
}

