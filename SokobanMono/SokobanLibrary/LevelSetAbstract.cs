﻿using System;
using System.Collections;
using System.Runtime.Remoting.Lifetime;

using RemotingLifeTimeTools;

namespace Sokoban.Library
{
    public abstract class LevelSetAbstract : MarshalByRefObjectWithLease, ISponsable, ILevelSetProperties
    {
        // Collection of the levels in a XML level set
        protected ArrayList levels = new ArrayList();

        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string email = string.Empty;
        protected string url = string.Empty;
        protected string author = string.Empty;
        protected string filename = string.Empty;

        protected int currentLevel = 0;
        protected int nrOfLevelsInSet = 0;
        protected int lastFinishedLevel = 0;

        #region Properties

        public string Title
        {
            get { return title; }
        }

        public string Description
        {
            get { return description; }
        }

        public string Email
        {
            get { return email; }
        }

        public string Url
        {
            get { return url; }
        }

        public string Author
        {
            get { return author; }
        }

        public string Filename
        {
            get { return filename; }
        }

        public int NrOfLevels
        {
            get { return nrOfLevelsInSet; }
        }

        public int CurrentLevel
        {
            get { return currentLevel; }
            set { currentLevel = value; }
        }

        public virtual int LastFinishedLevel
        {
            set { lastFinishedLevel = value; }
			get { return lastFinishedLevel; }
        }

        #endregion

        public virtual LevelAbstract this[int index]
        {
            get { return (LevelAbstract) levels[index]; }
        }
		
		
		#region Implemented Methods
		
		
		public virtual LevelDescription[] LevelsInfos
		{
			get
			{
				LevelDescription[] infos = new LevelDescription[nrOfLevelsInSet];
				for (int i=0; i<infos.Length; i++)
				{
					infos[i] = this[i].GetDescription();
				}
				return infos;
			}
		}
		
		public LevelSetDescription GetDescription()
		{
			return new LevelSetDescription(title, description, email, url, author, nrOfLevelsInSet);	
		}
		
		#endregion

    }
}
