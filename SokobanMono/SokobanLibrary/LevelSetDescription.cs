using System;
namespace Sokoban.Library
{
		[Serializable]
		public struct LevelSetDescription{
			public readonly string Title;
	        public readonly string Description;
	        public readonly string Email;
	        public readonly string Url;
	        public readonly string Author;
			public readonly int NrOfLevelsInSet;
			
			internal LevelSetDescription(string title, string description, string email, string url, string author, int nrOfLevelsInSet)
			{
				this.Title  = title;
				this.Description = description;
				this.Email = email;
				this.Url = url;
				this.Author = author;
				this.NrOfLevelsInSet = nrOfLevelsInSet;
			}
		}
}

