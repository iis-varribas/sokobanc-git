using System;
namespace Sokoban.Library
{
	[Serializable]
	public struct LevelSnapshot
	{
		public readonly int moves, pushes;
		public readonly int posX, posY;
		public readonly Item[] cells;
		public readonly MoveDirection direction;
		
		public LevelSnapshot(int moves, int pushes, Item[] levelcells, MoveDirection dir, int posX, int posY)
		{
			this.moves  = moves; 
			this.pushes = pushes;
			this.direction = dir;
			this.posX = posX;
			this.posY = posY;
			this.cells = levelcells;
			
		}
	}
}

