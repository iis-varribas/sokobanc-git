﻿using System;

namespace Sokoban.Library
{
    /// <summary>
    /// Enum for the possible directions of Sokoban
    /// </summary>
    [Serializable]
    public enum MoveDirection
    {
        Right,
        Up,
        Down,
        Left
    }
}
