#!/bin/sh

# Copyright 2013
# Author: Victor Arribas (v.arribas.urjc@gmail.com)
# version: 12/01/2013

# Use cleanner.sh for clean build dir
# Use cleanner.sh ALL for also remove build tree

########################
project='*'
build_dir="$project/bin"
########################


f_all_clean(){
	ls $build_dir 1>/dev/null 2>&1 && \
	for build in $(ls -d1 $build_dir)
	do
		echo "rm '$build'"
		rm -r "$build"
	done
}


f_file_clean(){
	ls $build_dir 1>/dev/null 2>&1 && \
	for bin_dir in $(find $build_dir -type f)
	do
		echo "rm '$bin_dir'"
		rm "$bin_dir"
	done
}


case $1 in
ALL)
	f_all_clean
	;;
*)
	f_file_clean
	;;
esac



