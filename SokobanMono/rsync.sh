#!/bin/sh

# Copyright 2013
# Author: Victor Arribas (v.arribas.urjc@gmail.com)
# version: 12/01/2013

# Remember rsync and slash behaviour
L_DIR='../SokobanMono'
R_DIR='ADRH/'

USER='varribas'
HOSTS='
iota15.aulas.gsyc.es
beta15.aulas.gsyc.es
'

for HOST in $HOSTS
do
	rsync -avz "$L_DIR" ${USER}@${HOST}:"$R_DIR"
done

