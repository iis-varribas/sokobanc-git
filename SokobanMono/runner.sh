#!/bin/sh

# Copyright 2013
# Author: Victor Arribas (v.arribas.urjc@gmail.com)
# version: 13/01/2013

SLEEP_TIME=5

set -e
set -u
set +x

case $1 in
	r|R|Release)
		Mode="Release"
	;;
	d|D|Debug)
		Mode="Debug"
	;;
esac



if which xterm ; then
	XTERM=xterm
elif which x-terminal-emulator; then
	XTERM=x-terminal-emulator
fi


f_run_project(){
	$XTERM -e "cd ${1}/bin/${Mode} && ./${1}.exe ; read -d '' -t 0.1 -n 1000; echo 'Program finished. Press Enter to exit'; read" &
}

f_test_project(){
	[ -f "${1}/bin/${Mode}/${1}.exe" ] || return 1
}

for entry in $(ls -1)
do
	if [ -d "$entry" ]
	then
		if f_test_project $entry
		then
			echo "Founded $entry, launching..."
			f_run_project $entry
			sleep $SLEEP_TIME
		else
			echo "Founded $entry, but have not executable for ${Mode} mode."
		fi
	fi
done

